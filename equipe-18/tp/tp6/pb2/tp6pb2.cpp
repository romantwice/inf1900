/**
 * @file tp6pb2.cpp
 * @author Lylia Bouricha (lylia.bouricha@polymtl.ca) & Jeremie Leclerc (jeremie.leclerc@polymtl.ca)
 * @version 0.1
 * @date 2022-10-06
 *
 * @copyright Copyright (c) 2022
 * Titre : Capteurs et conversion analogique/numérique
 */

// Table des etats:
// A faire si necessaire

/* Identifications matérielles (Broches I/O)
+--------+---------------+--------------------+
| Broche | Entree/Sortie |   Description      |
+--------+---------------+--------------------+
| B0     | Sortie        | Led1               |
| B1     | Sortie        | Led1               |
| A0     | Entree        | Circuit analogique |
+--------+---------------+--------------------+ */

#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <can.h>

const uint8_t INPUT_ALL = 0x00;
const uint8_t OUTPUT_ALL = 0xff;
// Valeur seuil determiner manuellement avec un doigt sur la photoresistance et une flashlight
const uint8_t HIGH_THRESHOLD = 229; // 4.4V 
const uint8_t LOW_THRESHOLD = 156;  // 3.0V

void lightLedGreen()
{
    PORTB |= (1 << PB0);
    PORTB &= ~(1 << PB1);
}

void lightLedRed()
{
    PORTB |= (1 << PB1);
    PORTB &= ~(1 << PB0);
}

void lightLedAmber()
{
    lightLedGreen();
    _delay_ms(15);
    lightLedRed();
    _delay_ms(15);
}

int main()
{
    DDRA = INPUT_ALL;
    DDRB = OUTPUT_ALL;
    can can1 = can(); 
    uint8_t input_8b;

    while (true)
    {
        uint16_t input = can1.lecture(PA0);
        input = input >> 2; // On garde seulement les 8 bits les plus significatifs
        input_8b = static_cast<uint8_t>(input);

        if (input_8b < LOW_THRESHOLD)
        { // Lumiere basse
            lightLedGreen();
        }
        else if (input_8b > HIGH_THRESHOLD)
        { // Lumiere haute
            lightLedRed();
        }
        else
        { // Lumiere ambiante
            lightLedAmber();
        }
    }

    return 0;
}
