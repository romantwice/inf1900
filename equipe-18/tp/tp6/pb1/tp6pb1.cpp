/**
 * @file tp6pb1.cpp
 * @author Lylia Bouricha (lylia.bouricha@polymtl.ca) & Jeremie Leclerc (jeremie.leclerc@polymtl.ca)
 * @version 0.1
 * @date 2022-10-06
 *
 * @copyright Copyright (c) 2022
 * Titre : Capteurs et conversion analogique/numérique
 */
/* Quand le bouton est enfoncé, un compteur qui incrémente 10 fois par seconde est activé.
Quand le bouton est relâché ou lorsque le compteur atteint 120, la lumière clignote
vert pendant 1/2 seconde. Ensuite, la carte mère ne fait rien. Puis, deux secondes
plus tard, la lumière rouge s'allume. Elle devra clignoter (compteur / 2) fois au
rythme de 2 fois par seconde. Ensuite, la lumière tourne au vert pendant une seconde.
Finalement, elle s'éteint et le robot revient à son état original. */

// Table des etats:
/* +----------+---+
| Sortie Z |   |
+----------+---+
| E0       | 0 |
| E1       | 0 |
| E2       | 1 |
| E3       | 0 |
| E4       | 2 |
| E5       | 3 |
+----------+---+ */
// 0 = LED ETEINT
// 1 = LED VERT CLIGNOTANT
// 2 = LED ROUGE CLIGNOTANT
// 3 = LED VERT

/* +--------------+----+--------------+
| ETAT PRESENT | D2 | ETAT SUIVANT |
+--------------+----+--------------+
| E0           | 1  | E1           |
| E1           | X  | E2           |
| E2           | X  | E3           |
| E3           | X  | E4           |
| E4           | X  | E5           |
| E5           | X  | E0           |
+--------------+----+--------------+ */



/* Identifications matérielles (Broches I/O)
+--------+---------------+--------------------+
| Broche | Entree/Sortie |   Description      |
+--------+---------------+--------------------+
| B0     | Sortie        | Led1               |
| B1     | Sortie        | Led1               |
| A0     | Entree        | Circuit analogique |
+--------+---------------+--------------------+ */


#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#define PRESSED true
#define NOTPRESSED false

const uint8_t INPUT_ALL = 0x00;
const uint8_t OUTPUT_ALL = 0xff;
const uint16_t COUNTER_MAX1 = 120;
const uint16_t COUNTER_MAX2 = 5;
const uint16_t TIMER_FACTOR = 781; // 8 000000/1024/10 = freq/timer prescaler/nStep par seconde voulu
volatile uint8_t gIsbuttonPressed = 0;
volatile uint8_t gExpiredTimer = 0;
volatile uint16_t gCounter = 0;
volatile uint16_t gFinalCounter = 0;

enum class State
{
    State0,
    State1,
    State2,
    State3,
    State4,
    State5
};

void ledOff()
{
    PORTB &= ~(1 << PB1);
    PORTB &= ~(1 << PB0);
}

void lightLedGreen()
{
    PORTB |= (1 << PB0);
    PORTB &= ~(1 << PB1);
}

void lightLedRed()
{
    PORTB |= (1 << PB1);
    PORTB &= ~(1 << PB0);
}

void chooseColor(State state)
{
    switch (state)
    {
    case State::State0:
        ledOff();
        break;

    case State::State1:
        ledOff();
        break;
    case State::State2:
        lightLedGreen();
        _delay_ms(100);
        ledOff();
        _delay_ms(100);
        break;
    case State::State3:
        ledOff();
        break;
    case State::State4:
        for (uint16_t i = 0; i < (gFinalCounter / 2); i++) 
        {
            lightLedRed();
            _delay_ms(250);
            ledOff();
            _delay_ms(250);
        }
        break;
    case State::State5:
        lightLedGreen();
        _delay_ms(1000);
        break;

    default:
        break;
    }
}

//a mettre dans librairie
void startTimer()
{
    gExpiredTimer = 0;
    // mode CTC du timer 1 avec horloge divisée par 1024
    // interruption après la durée spécifiée
    TCNT1 = 0;
    OCR1A = TIMER_FACTOR; 
    TCCR1A = 0;
    TCCR1B = (1 << WGM12) | (1 << CS12) | (1 << CS10);
    TCCR1C = 0;
    TIMSK1 = 1 << OCIE1A;
}

//dans librairie
void stopTimer()
{
    TCCR1B &= ~((1 << WGM12) | (1 << CS12) | (1 << CS10));
}

State nextState(State state)
{
    switch (state)
    {
    case State::State0:
        if (gIsbuttonPressed)
        {
            state = State::State1;
            startTimer();
        }
        break;
    case State::State1:
        if (!gIsbuttonPressed || gCounter >= COUNTER_MAX1)
        {
            state = State::State2;
            gFinalCounter = gCounter;
            gCounter = 0;
        }
        break;
    case State::State2:
        if (gCounter >= COUNTER_MAX2)
        {
            state = State::State3;
            stopTimer();
        }
        break;
    case State::State3: // ne fait rien pendant 2 sec
        _delay_ms(2000);
        state = State::State4;
        break;
    case State::State4:
        state = State::State5;
        break;
    case State::State5:
        state = State::State0;
        break;
    }
    return state;
}

ISR(TIMER1_COMPA_vect)
{
    gCounter++;
}

ISR(INT0_vect)
{
    // anti-rebond
    bool x0 = PORTD & (1 << PIND2);
    _delay_ms(30);
    bool x1 = PORTD & (1 << PIND2);

    if (x1 != x0)
    {
        return;
    }

    gIsbuttonPressed = !gIsbuttonPressed;
    
}

void initializeRegister(void)
{
    cli();

    DDRB = OUTPUT_ALL;
    DDRD = INPUT_ALL;

    EIMSK |= (1 << INT0);

    EICRA |= (1 << ISC00);
    EICRA &= ~(1 << ISC01);

    sei();
}

int main()
{

    initializeRegister();
    State state = State::State0;

    while (true)
    {
        state = nextState(state);
        chooseColor(state);
    };

    return 0;
}
