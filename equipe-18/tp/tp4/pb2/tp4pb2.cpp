#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#define PRESSED   true
#define BUTTONPIN 0x04
#define LED_OFF   0x00
#define LED_GREEN 0x01
#define LED_RED   0x02
#define ONESECOND 0x1E84 // F_CPU/1024

enum class State
{
  INIT,
  EA,
  EB,
  EC,
  ED
};

const uint8_t ENTREE = 0x00;
const uint8_t SORTIE = 0xff;
volatile uint8_t gExpiredTimer;
volatile uint8_t gPushButton;
volatile State currentState{State::INIT};

void setLedAmber() // fonction pour allumer la led couleur ambre
{
  PORTA = LED_RED;
  _delay_ms(5);
  PORTA = LED_GREEN;
  _delay_ms(5);
}


void startTimer(uint16_t duration)
{
  gExpiredTimer = 0;
  // mode CTC du timer 1 avec horloge divisée par 1024
  // interruption après la durée spécifiée
  TCNT1 = 0;
  OCR1A = duration;
  TCCR1A = 0b00000000; //(1 << )
  TCCR1B = 0b00001101;
  TCCR1C = 0; 
  TIMSK1 = 0b00000010;
}

// placer le bon type de signal d'interruption
// à prendre en charge en argument

ISR(TIMER1_COMPA_vect)
{
  gExpiredTimer = 1;
  PORTA = LED_RED;
  _delay_ms(1000);
}

ISR(INT0_vect)
{
  // anti-rebond
  _delay_ms(30);
  gPushButton = 1;


}

void initialisation(void)
{
  // cli est une routine qui bloque toutes les interruptions.
  // Il serait bien mauvais d'être interrompu alors que
  // le microcontrôleur n'est pas prêt...
  cli();
  // configurer et choisir les ports pour les entrées
  // et les sorties. DDRx... Initialisez bien vos variables
  DDRA = SORTIE; // PORT A est en mode sortie
  DDRD = ENTREE; //  PORT D est en mode entree
  // cette procédure ajuste le registre EIMSK
  // de l’ATmega324PA pour permettre les interruptions externes
  EIMSK |= (1 << INT0);
  // il faut sensibiliser les interruptions externes aux
  // changements de niveau du bouton-poussoir
  // en ajustant le registre EICRA
  EICRA |= (1 << ISC00);
  EICRA &= ~(1 << ISC01);
  // sei permet de recevoir à nouveau des interruptions.

  sei();
}

int main()
{
  initialisation();
  PORTA = LED_OFF;
  _delay_ms(5000);
  PORTA = LED_RED;
  _delay_ms(100);
  PORTA = LED_OFF;
  gPushButton = !PRESSED;
  startTimer(ONESECOND);

  do {

  // attendre qu'une des deux variables soit modifiée
  // par une ou l'autre des interruptions.

  } while ( gExpiredTimer == 0 && gPushButton == 0 );


  // Une interruption s'est produite. Arrêter toute
  // forme d'interruption. Une seule réponse suffit.

  cli ();
  // Verifier la réponse

  if(gExpiredTimer) 
  {
    PORTA = LED_RED;
  } else if (gPushButton)
  {
    PORTA = LED_GREEN;
  }


  return 0;
}