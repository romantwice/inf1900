#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#define PRESSED         true
#define PUSHBUTTONPIN   0x04 // PIN D2
#define LED_OFF         0x00
#define LED_GREEN       0x01
#define LED_RED         0x02

enum class State
{
  INIT,
  EA,
  EB,
  EC,
  ED,
  EE
};

const uint8_t ENTREE = 0x00;
const uint8_t SORTIE = 0xff;
volatile uint8_t gIsbuttonPressed = !PRESSED;
volatile State currentState{State::INIT};


void setLedAmber() // fonction pour allumer la led couleur ambre
{
  PORTA = LED_RED;
  _delay_ms(5);
  PORTA = LED_GREEN;
  _delay_ms(5);
}

// placer le bon type de signal d'interruption
// à prendre en charge en argument

ISR(INT0_vect)
{

  // laisser un délai avant de confirmer la réponse du
  // bouton-poussoir: environ 30 ms (anti-rebond)

  _delay_ms(30);

  // se souvenir ici si le bouton est pressé ou relâché

  gIsbuttonPressed = PIND & PUSHBUTTONPIN; 

  // changements d'état tels que ceux de la
  // semaine précédente

  switch (currentState)
  {
  case State::INIT:
  if(gIsbuttonPressed){
    currentState=State::EA;
  }
    break;

  case State::EA:
    if(!gIsbuttonPressed){
    currentState=State::EB;
  }
    break;

  case State::EB:
    if(gIsbuttonPressed){
    currentState=State::EC;
  }
    break;

  case State::EC:
    if(!gIsbuttonPressed){
    currentState=State::ED;
  }
    break;

  case State::ED:
    if(gIsbuttonPressed){
    currentState=State::EE;
  }
    break;

  case State::EE:
    if(!gIsbuttonPressed){
    currentState=State::INIT;
  }
    break;
  }

  // Voir la note plus bas pour comprendre cette instruction et son rôle
  EIFR |= (1 << INTF0);
}

void initialisation(void) 
{
  // cli est une routine qui bloque toutes les interruptions.
  // Il serait bien mauvais d'être interrompu alors que
  // le microcontrôleur n'est pas prêt...
  cli();
  // configurer et choisir les ports pour les entrées
  // et les sorties. DDRx... Initialisez bien vos variables
  DDRA = SORTIE; // PORT A est en mode sortie
  DDRD = ENTREE; //  PORT D est en mode entree
  // cette procédure ajuste le registre EIMSK
  // de l’ATmega324PA pour permettre les interruptions externes
  EIMSK |= (1 << INT0);
  // il faut sensibiliser les interruptions externes aux
  // changements de niveau du bouton-poussoir
  // en ajustant le registre EICRA
  EICRA |= (1<<ISC00);
  EICRA &= ~(1<<ISC01);
  // sei permet de recevoir à nouveau des interruptions.

  sei();
}

int main()
{
  initialisation();

  for (;;) // boucle sans fin
  {

    switch (currentState)
    {
    case State::INIT:
      PORTA = LED_RED;
      break;

    case State::EA:
      setLedAmber(); 
      break;

    case State::EB:
      PORTA = LED_GREEN;
      break;

    case State::EC:
      PORTA = LED_RED;
      break;

    case State::ED:
      PORTA = LED_OFF;
      break;

    case State::EE:
      PORTA = LED_GREEN;
      break;
    }
  }
  return 0;
}