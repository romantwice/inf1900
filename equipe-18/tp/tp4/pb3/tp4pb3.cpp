#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#define LED_OFF   0x00
#define LED_GREEN 0x01
#define LED_RED   0x02

const uint8_t ENTREE = 0x00;
const uint8_t SORTIE = 0xff;

//librairie classe timer
void ajustementPwm ( uint8_t duration ) {

// mise à un des sorties OC1A et OC1B sur comparaison
// réussie en mode PWM 8 bits, phase correcte
// et valeur de TOP fixe à 0xFF (mode #1 de la table 16-5
// page 130 de la description technique du ATmega324PA)
OCR1A = duration;
OCR1B = duration;
// division d'horloge par 8 - implique une fréquence de PWM fixe
//TCCR1A = 0b11110001; //a changer!! 
TCCR1A |= (1 << WGM10) | (0 << COM1A0) | (1 << COM1A1) | (0<< COM1B0) | (1 << COM1B1) ;
//TCR1B=0b00000010;
TCCR1B |= (1 << CS11);
TCCR1C = 0;
} 


int main()
{
  DDRD=SORTIE;
  ajustementPwm(0);
  _delay_ms(2000);

  ajustementPwm(0x40);
  _delay_ms(2000);

  ajustementPwm(0x80);
  _delay_ms(2000);

  ajustementPwm(0xC0);
  _delay_ms(2000);

  ajustementPwm(0xFF);
  _delay_ms(2000);
   ajustementPwm(0);

  return 0;
}