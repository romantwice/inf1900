
#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <math.h>

const int SIGNALPERIOD1 = 167; // 1/60*10000
const int SIGNALPERIOD2 = 25; // 1/400*10000
const uint8_t LED_OFF = 0x00;
const uint8_t LED_GREEN = 0x01;
const uint8_t LED_RED = 0x02;
const bool MOTORRUN = 0x01;
const bool MOTOROFF = 0x00;
const bool DIRECTION = 0;
const int INCREASERATE = 25; 
const int TIMEMOTORDELAY = 2000; //ms

const int TIMEMOTORDELAYTEST=20; // ms apres les %

const int TIMEOFLIFE = 10; //ms
const int DELAYFACTOR = 10000;

  void delayIt(int delayIteration)
  {
    for (int i = 0; i < delayIteration; i++)
    {
      _delay_us(0.01);
    }
    PORTA = LED_GREEN;
  }

  void motorRun(int delaySignalHigh, int signalPeriod)
  {
    int i = 200;
    while (i > 0)
    {
      i--;
      PORTB |=(1<<PB0);
      PORTA = LED_RED;
      delayIt(delaySignalHigh);
      //PORTB &= ~(1<<PB0);
      PORTA = LED_OFF;
      //delayIt(signalPeriod - delaySignalHigh);
      delayIt(TIMEMOTORDELAY - delaySignalHigh);
    }
  }

int main()
{
  DDRA = 0xff; // PORT A est en mode sortie
  DDRB = 0xff; // PORT B est en mode sortie
  PORTB |= (1<<PB1);
  int i = TIMEOFLIFE;
  bool firstLife = true;
  int motorRate = 0;
  int signalPeriod = SIGNALPERIOD1;

  for (;;) // boucle sans fin
  {
    if (i > 0) // 10 secondes de vie
    {
      int delaySignalHigh = TIMEMOTORDELAYTEST*motorRate;
      //int delaySignalHigh = signalPeriod;
      motorRun(delaySignalHigh, signalPeriod);
      motorRate += INCREASERATE;
      i--;
    } else if(i==0 && firstLife)
    {
     
       motorRate=0;
        i = TIMEOFLIFE;
        signalPeriod = SIGNALPERIOD2;
        firstLife=false;
    }
    else if (i==0 && !firstLife)
    {
      PORTA=LED_GREEN;
    }
  }
  
  return 0;
}