
#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <math.h>

const int b = 1000;
const int delay_led = 3000; // 3 sec.
const uint8_t LED_OFF = 0x0;
const uint8_t LED_GREEN = 0x1;
const uint8_t LED_RED = 0x2;

  void delayIt(int delayIteration)
  {
    for (int i = 0; i <= delayIteration; i++)
    {
      _delay_us(1);
    }
  }

int main()
{
  DDRA = 0xff; // PORT A est en mode sortie
  int a = b;
  int i = delay_led;
  uint8_t compteur = 0x00;

  for (;;) // boucle sans fin
  {
    
    if (i > 0) // 3 secondes d'attenuation
    {
      i--;
      PORTA = LED_GREEN;
      delayIt(a);
      compteur++;
      if (compteur & 0x03)
      {
        compteur = 0x00;
        a--;
      }
    }
    PORTA = LED_OFF;
    delayIt((b-a));
  }
  return 0;
}