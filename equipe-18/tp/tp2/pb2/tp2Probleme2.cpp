/**
 * @file tp2Probleme2.cpp
 * @author Lylia Bouricha (lylia.bouricha@polymtl.ca) & Jeremie Leclerc (jeremie.leclerc@polymtl.ca)
 * @version 0.1
 * @date 2022-09-08
 *
 * @copyright Copyright (c) 2022
 * Titre : Machines a etats finis logicielles
 */

// Table des etats: Machine de Mealy
/*+---------------+------------------+------------+--------+
| Current State |      Input       | Next State | Output |
+---------------+------------------+------------+--------+
| Init          | ButtonNotPressed | INIT       | RED    |
| Init          | ButtonPressed    | EA         | AMBER  |
| EA            | ButtonNotPressed | EA         | GREEN  |
| EA            | ButtonPressed    | EB         | RED    |
| EB            | ButtonNotPressed | EB         | OFF    |
| EB            | ButtonPressed    | INIT       | GREEN  |
+---------------+------------------+------------+--------+*/

/* Identifications matérielles (Broches I/O)
+--------+---------------+-----------------+
| Broche | Entree/Sortie |   Description   |
+--------+---------------+-----------------+
| A0     | Sortie        | Led1            |
| A1     | Sortie        | Led1            |
| D4     | Entree        | Bouton poussoir |
+--------+---------------+-----------------+ */

#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>

enum class State
{
    INIT,
    EA,
    EB
};

const uint8_t ENTREE = 0x00;
const uint8_t SORTIE = 0xff;
const double DEBOUNCE_DELAY = 10;
const double LED_DELAY = 5;
const uint8_t LED_OFF = 0x0;
const uint8_t LED_GREEN = 0x1;
const uint8_t LED_RED = 0x2;

bool isButtonPressed()
{
    if (PIND & 0x04)
    {
        _delay_ms(DEBOUNCE_DELAY);
        while (PIND & 0x04)
        {
            return true;
        }
    }
    return false;
}

void setLedAmber()
{
    PORTA = LED_RED;
    _delay_ms(LED_DELAY);
    PORTA = LED_GREEN;
}

int main()
{

    DDRA = SORTIE; // PORT A est en mode sortie
    DDRD = ENTREE; //  PORT D est en mode entree
    State currentState{State::INIT};

    for (;;) // boucle sans fin
    {

        switch (currentState)
        {
        case State::INIT:
            PORTA = LED_RED;
            while (isButtonPressed())
            {
                currentState = State::EA;
                setLedAmber();
            }

            break;
        case State::EA:
            PORTA = LED_GREEN;
            while (isButtonPressed())
            {
                currentState = State::EB;
                PORTA = LED_RED;
            }

            break;
        case State::EB:
            PORTA = LED_OFF;
            while (isButtonPressed())
            {
                currentState = State::INIT;
                PORTA = LED_GREEN;
            }
            break;
        }
    }
    return 0;
}