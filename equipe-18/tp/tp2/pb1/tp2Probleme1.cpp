/**
 * @file tp2Probleme1.cpp
 * @author Lylia Bouricha (lylia.bouricha@polymtl.ca) & Jeremie Leclerc (jeremie.leclerc@polymtl.ca)
 * @version 0.1
 * @date 2022-09-08
 *
 * @copyright Copyright (c) 2022
 * Titre : Machines a etats finis logicielles
 */
// Table des Etats:Machine de Moore

/*+---------------+------------------+------------+--------+
| Current State |      Input       | Next State | Output |
+---------------+------------------+------------+--------+
| Init          | ButtonNotPressed | Init       | OFF    |
| Init          | ButtonPressed    | EA         | OFF    |
| EA            | ButtonNotPressed | EA         | OFF    |
| EA            | ButtonPressed    | EB         | OFF    |
| EB            | ButtonNotPressed | EB         | OFF    |
| EB            | ButtonPressed    | EC         | OFF    |
| EC            | ButtonNotPressed | EC         | OFF    |
| EC            | ButtonPressed    | Init       | GREEN  |
|               |                  |            |        |
+---------------+------------------+------------+--------+*/

/* Identifications matérielles (Broches I/O)
+--------+---------------+-----------------+
| Broche | Entree/Sortie |   Description   |
+--------+---------------+-----------------+
| A0     | Sortie        | Led1            |
| A1     | Sortie        | Led1            |
| D4     | Entree        | Bouton poussoir |
+--------+---------------+-----------------+ */

#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>

enum class State
{
    INIT,
    EA,
    EB,
    EC
};

const uint8_t ENTREE = 0x00;
const uint8_t SORTIE = 0xff;
const double DEBOUNCE_DELAY = 10;
const uint8_t LED_INIT = 0x0;
const uint8_t LED_GREEN = 0x1;
const double LED_DELAY = 2000;

bool isButtonPressed()
{
    if (PIND & 0x04)
    {
        _delay_ms(DEBOUNCE_DELAY);
        while (PIND & 0x04)
        {
            return true;
        }
    }
    return false;
}

int main()
{

    DDRA = SORTIE; // PORT A est en mode sortie
    DDRD = ENTREE; //  PORT D est en mode entree
    State currentState{State::INIT};

    for (;;) // boucle sans fin
    {

        switch (currentState)
        {
        case State::INIT:
            PORTA = LED_INIT;
            while (isButtonPressed())
            {
                currentState = State::EA;
            }

            break;
        case State::EA:
            while (isButtonPressed())
            {
                currentState = State::EB;
            }

            break;
        case State::EB:
            while (isButtonPressed())
            {
                currentState = State::EC;
            }
            break;
        case State::EC:
            if (!(isButtonPressed()))
            {
                PORTA = LED_GREEN;
                _delay_ms(LED_DELAY);
                currentState = State::INIT;
            }
            break;
        }
    }
    return 0;
}