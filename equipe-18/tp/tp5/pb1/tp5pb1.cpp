/**
 * @file tp5pb1.cpp
 * @author Lylia Bouricha (lylia.bouricha@polymtl.ca) & Jeremie Leclerc (jeremie.leclerc@polymtl.ca)
 * @version 0.2
 * @date 2022-09-29
 *
 * @copyright Copyright (c) 2022
 * Titre : lecture et écriture en mémoire externe
 */

// Table des etats: 
// A faire si necessaire

// Identifications matérielles (Broches I/O)
// A faire

#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <memoire_24.h>

const uint8_t ENTREE = 0x00;
const uint8_t SORTIE = 0xff;
const uint8_t LED_OFF = 0x0;
const uint8_t LED_GREEN = 0x1;
const uint8_t LED_RED = 0x2;
const char chainePolytechniqueMontreal[44+1] = {'*','P','*','O','*','L',
                '*','Y','*','T','*','E','*','C','*','H','*','N','*','I',
                '*','Q','*','U','*','E','*','*','M','*','O','*','N','*',
                'T','*','R','*','E','*','A','*','L','*','\0'};
const uint16_t adressInit = 0x0000;

void lightLedGreen()
{
    PORTA |= (1 << PA0);
    PORTA &= ~(1 << PA1);
}

void lightLedRed()
{
    PORTA |= (1 << PA1);
    PORTA &= ~(1 << PA0);
}

void ledOff()
{
    PORTA &= ~((1 << PA0) | (1 << PA1));
}

void initialisationLed() // Verification du branchement de la led
{
    lightLedGreen();
    _delay_ms(2000);
    lightLedRed();
    _delay_ms(2000);
    ledOff();
}
int main()
{

    DDRA = SORTIE; // PORT A est en mode sortie
    DDRD = ENTREE; //  PORT D est en mode entree
    // initialisationLed(); // A executer a chaque branchement de la led
    uint8_t longueurChaine = sizeof(chainePolytechniqueMontreal);
    Memoire24CXXX memoire24;
    char chaineMemoire[44+1];
    bool chaineIdentique = true;
    memoire24.ecriture(adressInit, ((uint8_t*) chainePolytechniqueMontreal),
        longueurChaine);
    memoire24.lecture(adressInit, ((uint8_t*) chaineMemoire), longueurChaine);
    for (uint8_t i = 0; i < longueurChaine; i++)
    {
        if (chainePolytechniqueMontreal[i] != chaineMemoire[i])
        {
            chaineIdentique = false;
        }
    }
    if (chaineIdentique)
    {
        lightLedGreen();
    }
    else
    {
        lightLedRed();
    }
        


    return 0;
}
