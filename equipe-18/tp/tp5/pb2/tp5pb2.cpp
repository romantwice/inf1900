/**
 * @file tp5pb2.cpp
 * @author Lylia Bouricha (lylia.bouricha@polymtl.ca) & Jeremie Leclerc (jeremie.leclerc@polymtl.ca)
 * @version 0.1
 * @date 2022-09-29
 *
 * @copyright Copyright (c) 2022
 * Titre : Communication RS232
 */

// Table des etats: 
// A faire si necessaire

// Identifications matérielles (Broches I/O)
// A faire

#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>

const uint8_t ENTREE = 0x00;
const uint8_t SORTIE = 0xff;
const uint8_t LED_OFF = 0x0;
const uint8_t LED_GREEN = 0x1;
const uint8_t LED_RED = 0x2;

void lightLedGreen()
{
    PORTA |= (1 << PA0);
    PORTA &= ~(1 << PA1);
}

void lightLedRed()
{
    PORTA |= (1 << PA1);
    PORTA &= ~(1 << PA0);
}

void initialisationUART ( void ) {
// 2400 bauds. Nous vous donnons la valeur des deux
// premiers registres pour vous éviter des complications.
UBRR0H = 0;
UBRR0L = 0xCF;
// permettre la réception et la transmission par le UART0
UCSR0A = 0;
UCSR0B = (1<<RXEN0)|(1<<TXEN0);
// Format des trames: 8 bits, 1 stop bits, sans parité
UCSR0C = (1<<UCSZ00) | (1<<UCSZ01);
// UCSR0C &= ~(1<<UCPOL0);
}

// Du USART vers le PC
void transmissionUART ( uint8_t donnee ) {
    /* Wait for empty transmit buffer */
    while ( !( UCSR0A & (1<<UDRE0)) ) {};
    /* Put data into buffer, sends the data */
    UDR0 = donnee;
}

int main()
{
    DDRA |= (1 << PA0) | (1 << PA1);
    lightLedGreen();
    _delay_ms(2000);
    char mots[21] = "Le robot en INF1900\n";
    initialisationUART();
    uint8_t i, j;
    for ( i = 0; i < 1; i++ ) {
        for ( j=0; j < 20; j++ ) {
        lightLedRed();
        transmissionUART ( mots[j] );
        }

    }

    return 0;
}