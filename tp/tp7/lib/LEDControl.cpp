//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Implementation du controle de la DEL libre.

#include <LEDControl.h>

LEDControl::LEDControl(int pinIn, int pinOut)
{
    pinIn_ = pinIn;
    pinOut_ = pinOut;
}

void LEDControl::changeColor(Color color, volatile uint8_t &port_, int delay)
{

    switch (color)
    {
    case Color::RED:
        for (int i = delay; i > 0; i--)
        {
            port_ &= ~(1 << pinIn_);
            port_ |= 1 << pinOut_;
            _delay_ms(1);
        }
        break;

    case Color::AMBER:
    {
        // Le delay est divise par 7 parce que la couleur rouge et verte genere
        // chacun un delai specifique (1 et 6 dans ce cas).
        for (int i = delay / 7; i > 0; i--)
        {
            changeColor(Color::RED, port_, 1);
            changeColor(Color::GREEN, port_, 6);
        }
        break;
    }

    case Color::GREEN:
        for (int i = delay; i > 0; i--)
        {
            port_ &= ~(1 << pinOut_);
            port_ |= 1 << pinIn_;
            _delay_ms(1);
        }
        break;

    case Color::NONE:
        port_ &= (0 << pinIn_) & (0 << pinOut_);
        break;

    default:
        break;
    }
}
