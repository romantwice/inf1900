//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Definition de differents modes de la transmission UART.

#ifndef UART_H
#define UART_H

#include <EnumMode.h>
#include <avr/io.h>
class Uart
{
public:
    Uart(Mode mode);
    ~Uart();
    void uartInitializationTransmit();
    void uartInitializationReceive();
    void uartInitializationTransmitAndReceive();
    void uartTransmition(uint8_t data);
    unsigned char uartReceive(void);
    bool isUartBusy();
    void print(char word[]);

private:
};

#endif // !UART_H