//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Implementation de la classe timer1CTC.

#include "Timer1CTC.h"

Timer1CTC::Timer1CTC()
{
	initRegister();
}

Timer1CTC::~Timer1CTC()
{
}

void Timer1CTC::initRegister()
{
	TCNT1 = 0;
	OCR1A = 0;
	TCCR1A = 0;
	TCCR1B = (1 << WGM12);
	TCCR1C = 0;
	TIMSK1 = 1 << OCIE1A;
}

void Timer1CTC::startTimer(const uint16_t outputCompareValue)
{
	TCCR1B |= (1 << WGM12) | (1 << CS12) | (1 << CS10);
	OCR1A = outputCompareValue;
}

void Timer1CTC::stopTimer()
{
	TCCR1B &= ~((1 << WGM12) | (1 << CS12) | (1 << CS10));
}