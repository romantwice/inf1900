//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Definition du controle de la DEL libre.

#ifndef LEDCONTROL_H
#define LEDCONTROL_H

#ifndef F_CPU
#define F_CPU 8000000UL
#endif

#include <avr/io.h>
#include <util/delay.h>
#include <EnumColor.h>

class LEDControl
{
public:
    LEDControl(const int pinIn, const int pinOut);
    void changeColor(Color color, volatile uint8_t &port_, const int delay);

private:
    Color color_ = Color::NONE;
    int pinIn_;
    int pinOut_;
};

#endif // !LEDCONTROL_H