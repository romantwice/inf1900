//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Definition des differentes couleurs de la DEL libre.

#ifndef ENUMCOLOR_H
#define ENUMCOLOR_H

enum class Color
{
    RED,
    AMBER,
    GREEN,
    NONE
};

#endif // !ENUMCOLOR_H
