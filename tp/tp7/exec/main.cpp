//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Tester toutes les classes definies et implementees dans libstatique.a.

#define F_CPU 8000000UL
#include <avr/io.h>
#include <LEDControl.h>
#include <MotorPWM.h>
#include <util/delay.h>
#include <Uart.h>
#include <Button.h>
#include <Debug.h>

int main()
{
    // Simple test 
    Uart uart = Uart(Mode::RECEIVEANDTRANSMIT);
    DEBUG_PRINT(("Message a transmettre."))
}
