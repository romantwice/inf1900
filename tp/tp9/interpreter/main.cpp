//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Interprete les instructions inscrites sur la memoire externe de la carte a microcontroleur.

#define F_CPU 8000000UL
#include <avr/io.h>
#include <LEDControl.h>
#include <MotorPWM.h>
#include <util/delay.h>
#include <Uart.h>
#include <Button.h>
#include <Debug.h>
#include <Piezo.h>
#include <memory_24.h>
#include <Reader.h>
#include <Interpreteur.h>

void runStartSequence(LEDControl led, Piezo piezo)
{
    led.blink(Color::RED, PORTA, 3);
    piezo.startSound(55);
    _delay_ms(200);
    piezo.startSound(65);
    _delay_ms(200);
    piezo.startSound(48);
    _delay_ms(300);
    piezo.startSound(65);
    _delay_ms(200);
    led.changeColor(Color::GREEN, PORTA);
    piezo.stopSound();
}

int main()
{
    DDRD |= (1 << PD6) | (1 << PD7);
    DDRA |= (1 << PA0) | (1 << PA1);
    DDRB |= ((1 << PB2) | (1 << PB3) | (1 << PB5) | (1 << PB4)); // Mise en sortie OC0A et OC0B - PB1 et PB2 pour signal direction

    Interpreteur interpreteur = Interpreteur();
    LEDControl led = LEDControl(PA1, PA0);
    MotorPWM motorPwm = MotorPWM(PB2, PB5);
    Piezo piezo = Piezo(PD7);
    Memoire24CXXX memory = Memoire24CXXX();
    Reader reader = Reader();
    Uart uart = Uart(Mode::RECEIVEANDTRANSMIT);
    runStartSequence(led, piezo);
    _delay_ms(500);

    reader.readandInstruct(memory, interpreteur, uart, led, PORTA, motorPwm, PORTB, piezo);
    // reader.readAllMemory(memory, uart);
    return 0;
}


