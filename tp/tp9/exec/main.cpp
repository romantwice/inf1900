//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Tester toutes les classes definies et implementees dans libstatique.a.

#define F_CPU 8000000UL
#include <avr/io.h>
#include <LEDControl.h>
#include <MotorPWM.h>
#include <util/delay.h>
#include <Uart.h>
#include <Button.h>
#include <Debug.h>
#include <Piezo.h>
#include <memory_24.h>
#include <Reader.h>
#include <Interpreteur.h>

void initializePort()
{
    DDRA |= (1 << PA0) | (1 << PA1);	
    DDRB |= ((1 << PB2) | (1 << PB3) | (1 << PB5) | (1 << PB4)); //Mise en sortie OC0A et OC0B - PB1 et PB2 pour signal direction	
    DDRD |= (1 << PD6) | (1 << PD7);
}

int main()
{ 
    initializePort();
    //////// INITIALIZE CLASS //////////
    LEDControl led = LEDControl(PA1, PA0);
    Reader reader = Reader();
    Piezo piezo = Piezo(PD7);
    Uart uart = Uart(Mode::RECEIVEANDTRANSMIT);
    Memoire24CXXX memory = Memoire24CXXX();
    Interpreteur interpreteur = Interpreteur();
    MotorPWM motorPwm = MotorPWM(PB2, PB5);
    return 0;
}
