//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Programme des instructions du fichier instruct.txt sur la memoire externe.

#define F_CPU 8000000UL
#include <avr/io.h>
#include <LEDControl.h>
#include <MotorPWM.h>
#include <util/delay.h>
#include <Uart.h>
#include <Button.h>
#include <Debug.h>
#include <Piezo.h>
#include <memory_24.h>
#include <Reader.h>
#include <Interpreteur.h>

int main()
{

    Memoire24CXXX memory = Memoire24CXXX();
    Reader reader = Reader();
    Uart uart = Uart(Mode::RECEIVEANDTRANSMIT);

    reader.writeByteCodeInMemory(memory, uart);
    
    return 0;
}
