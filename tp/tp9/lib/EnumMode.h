//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Definition de differents modes de la transmission UART.

#ifndef ENUMMODE_H
#define ENUMMODE_H

enum class Mode
{
    RECEIVE,
    TRANSMIT,
    RECEIVEANDTRANSMIT
};

#endif // !ENUMMODE_H