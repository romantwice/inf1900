//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Implementation de la classe motorPWM.

#ifndef F_CPU
#define F_CPU 8000000UL
#endif

#include "MotorPWM.h"
#include <avr/io.h>

MotorPWM::MotorPWM(const int leftMotorPinDirection, const int rightMotorPinDirection)
{
	initRegister();
	leftMotorPinDirection_ = leftMotorPinDirection;
	rightMotorPinDirection_ = rightMotorPinDirection;
}

MotorPWM::~MotorPWM()
{
}

void MotorPWM::initRegister()
{
	TCCR0A |= (1 << WGM00) | (1 << COM0A1) | (1 << COM0B1);
	TCCR0A &= ~((1 << WGM01) | (1 << COM0A0) | (1 << COM0B0));
	TCCR0B |= (1 << CS01);
	TCCR0B &= ~((1 << CS00) | (1 << CS02) | (1 << WGM02));
}

void MotorPWM::ajustSpeed(uint8_t percentMotorLeft, uint8_t percentMotorRight)
{
	OCR0A = percentMotorLeft; // Correction of the difference in speeds of the motors
	OCR0B = percentMotorRight;
}

void MotorPWM::leftMotorRunForward(volatile uint8_t &port)
{
	port &= ~(1 << leftMotorPinDirection_);
}

void MotorPWM::leftMotorRunBackward(volatile uint8_t &port)
{
	port |= (1 << leftMotorPinDirection_);
}

void MotorPWM::rightMotorRunForward(volatile uint8_t &port)
{
	port &= ~(1 << rightMotorPinDirection_);
}

void MotorPWM::rightMotorRunBackward(volatile uint8_t &port)
{
	port |= (1 << rightMotorPinDirection_);
}

void MotorPWM::runStraightForward(const uint8_t speed, volatile uint8_t &port)
{
	rightMotorRunForward(port);
	leftMotorRunForward(port);
	ajustSpeed(speed, speed);
}

void MotorPWM::runStraightBackward(const uint8_t speed, volatile uint8_t &port)
{
	rightMotorRunBackward(port);
	leftMotorRunBackward(port);
	ajustSpeed(speed, speed);
}

void MotorPWM::turn90degreeRight(volatile uint8_t &port) // Blocking method because of delay
{
	leftMotorRunForward(port);
	ajustSpeed(0x80, 0x00);
	_delay_ms(2100);
	stopMotor(port);
}

void MotorPWM::turn90degreeLeft(volatile uint8_t &port) // Blocking method because of delay
{
	rightMotorRunForward(port);
	ajustSpeed(0x00, 0x80);
	_delay_ms(2100);
	stopMotor(port);
}

void MotorPWM::stopMotor(volatile uint8_t &port)
{
	ajustSpeed(0x00, 0x00);
}
