//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Definition de la classe button.

#pragma once
#ifndef BUTTON_H
#define BUTTON_H

#include <avr/io.h>
#include <util/delay.h>

class Button
{
public:
    bool didPress(volatile uint8_t pinToCheck, uint8_t mask) const;

private:
    const static int debouncingDelay_ = 10; // ms
};
#endif // !BUTTON_H