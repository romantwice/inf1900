//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Implementation de differents modes de la transmission UART.

#include <Uart.h>
/**
 * Constructor for Uart class. Initializes registers
 * param: Mode mode : allows us to choose a transmition/receiving/ transmition and receiving mode
 **/
Uart::Uart(Mode mode)
{
    switch (mode)
    {
    case Mode::RECEIVE:
        uartInitializationReceive();
        break;

    case Mode::TRANSMIT:
        uartInitializationTransmit();
        break;

    case Mode::RECEIVEANDTRANSMIT:
        uartInitializationTransmitAndReceive();
        break;
    }
}

/**
 * Destructor for Uart class.
 **/
Uart::~Uart() {}

/**
 * Initializes registers to receive and transmit data through UART0
 * Settings: 2400 baud, frames : 8 bits, 1 stop bit, without parity
 * param: void
 * return: void
 **/
void Uart::uartInitializationTransmit()
{
    UBRR0H = 0;
    UBRR0L = 0xCF;
    UCSR0A = 0;
    UCSR0B |= (1 << TXEN0);
    UCSR0C = (1 << UCSZ00) | (1 << UCSZ01);
}

/**
 * Initializes registers to receive data through UART0
 * Settings: 2400 baud, frames : 8 bits, 1 stop bit, without parity
 * param: void
 * return: void
 **/
void Uart::uartInitializationReceive()
{
    UBRR0H = 0;
    UBRR0L = 0xCF;
    UCSR0A = 0;
    UCSR0B |= (1 << RXEN0);
    UCSR0C |= (1 << UCSZ00) | (1 << UCSZ01);
}
/**
 * Initializes registers to receive and transmit data through UART0
 * Settings: 2400 baud, frames : 8 bits, 1 stop bit, without parity
 * param: void
 * return: void
 **/
void Uart::uartInitializationTransmitAndReceive()
{
    UBRR0H = 0;
    UBRR0L = 0xCF;
    UCSR0A = 0;
    UCSR0B |= (1 << RXEN0) | (1 << TXEN0);
    UCSR0C |= (1 << UCSZ00) | (1 << UCSZ01);
}

/**
 * Waits for the Uart to be available then puts data into buffer and sends it
 * param: uint8_t data : data sent from USART to PC
 * return: void
 **/
void Uart::uartTransmition(uint8_t data)
{
    while (isUartBusy())
    {
    };
    UDR0 = data;
}

/**
 * Waits for the data to be received
 * param: void
 * return: unsigned char containing the data received
 *
 * Function from the 8-bit Atmel Microcontroller with 16/32/64/128K Bytes In-System Programmable Flash Documentation 2015, section: 19.8.1, page: 177
 **/
unsigned char Uart::uartReceive(void)
{
    while (!(UCSR0A & (1 << RXC0)))
        ;
    return UDR0;
}

/**
 * Checks if the Uart is busy or not
 * param: none
 * return: boolean value if the uart is busy or not
 **/
bool Uart::isUartBusy()
{
    return !(UCSR0A & (1 << UDRE0));
}

void Uart::print(char word[])
{
    uint8_t i = 0;
    do
    {
        // uartTransmition(word[i++]);
    } while (word[i] != '\0');
}