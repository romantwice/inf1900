//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Implementation de la classe Interpreteur.

#include <Interpreteur.h>
// int i : paire = instruction, impair = operande, on aasinge a une valeur insutrction puis byter code et ensuite appel a
//  operandinterpreter(pas forcement necessaire) puis a instruction interpreter

Interpreteur::Interpreteur()
{
}

Interpreteur::~Interpreteur()
{
}

void Interpreteur::instructionInterpreteur(uint8_t instructionByteCode, uint8_t operandByteCode, LEDControl &led, volatile uint8_t &portLed,
                                           MotorPWM &motor, volatile uint8_t &portMotor, Piezo &piezo)
{
    switch (instructionByteCode)
    {
    case 0x01:
        // Start
   
        break;

    case 0x02:
        // Delay
        delay25(operandByteCode);
        break;

    case 0x44:
        // Turn LED green or red
        if (operandByteCode == 1)
        {
            led.changeColor(Color::GREEN, portLed);
        }
        else if (operandByteCode == 2)
        {
            led.changeColor(Color::RED, portLed);
        }
        break;

    case 0x45:
        // Turn off LED
        led.changeColor(Color::NONE, portLed);
        break;

    case 0x48:
        // Play sound
        piezo.startSound(operandByteCode);
        break;

    case 0x09:
        // Stop sound
        piezo.stopSound();
        break;

    case 0x60:
        // Stop motor
        // Function same as next one
    case 0x61:
        motor.stopMotor(PORTB);
        break;

    case 0x62:
        // Go forward
        motor.runStraightForward(operandByteCode, portMotor);
        break;

    case 0x63:
        // Go backwards
        motor.runStraightBackward(operandByteCode, portMotor);
        break;

    case 0x64:
        // Turn right
        motor.turn90degreeRight(portMotor);
        break;

    case 0x65:
        // Turn left
        motor.turn90degreeLeft(portMotor);
        break;

    case 0xC0:
        break;

    case 0xC1:

        break;

    case 0xFF:
        piezo.stopSound();
        led.changeColor(Color::NONE, portLed);
        motor.stopMotor(PORTB);
        led.changeColor(Color::NONE, portLed);
        break;
    }
}

void Interpreteur::delay25(uint8_t nTimes) const
{
    for (uint8_t i = 0; i < nTimes; i++)
    {
        _delay_ms(25);
    }
}
