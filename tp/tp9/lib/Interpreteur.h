//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Definition de la classe Interpreteur.

#ifndef INTERPRETEUR_H
#define INTERPRETEUR_H

#include <Uart.h>
#include <LEDControl.h>
#include <MotorPWM.h>
#include <Piezo.h>
#include <avr/io.h>
#include <memory_24.h>

class Interpreteur
{
public:
    Interpreteur();
    ~Interpreteur();
    void instructionInterpreteur(uint8_t instructionByteCode, uint8_t operandByteCode, LEDControl &led, volatile uint8_t &portLed,
                                 MotorPWM &motor, volatile uint8_t &portMotor, Piezo &piezo); // ou char
    void delay25(uint8_t nTimes) const;

private:
    int delay_ = 25;
    // int loopCounter_ = 0; // plus besoin puisque dans reader
};

#endif //! INTERPRETEUR_H