//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Implementation du controle de la DEL libre.

#include <LEDControl.h>

LEDControl::LEDControl(int pinIn, int pinOut)
{
    pinIn_ = pinIn;
    pinOut_ = pinOut;
}

LEDControl::LEDControl()
{
    pinIn_ = 0;
    pinOut_ = 0;
}

LEDControl::~LEDControl()
{
}

void LEDControl::changeColor(Color color, volatile uint8_t &port_) const
{

    switch (color)
    {
    case Color::RED:
        port_ &= ~(1 << pinIn_);
        port_ |= 1 << pinOut_;
        break;
    case Color::AMBER:
    {
        for (int i = 1000; i > 0; i--)
        {
            // changeColor(Color::RED, port_);
            // _delay_ms(15);
            // changeColor(Color::GREEN, port_);
            // _delay_ms(15)
        }
        break;
    }

    case Color::GREEN:
        port_ &= ~(1 << pinOut_);
        port_ |= 1 << pinIn_;
        break;

    case Color::NONE:
        port_ &= (0 << pinIn_) & (0 << pinOut_);
        break;

    default:
        break;
    }
}

void LEDControl::blink(Color color, volatile uint8_t &port_, int nBlink) const
{
    for (int i = 0; i < nBlink; i++)
    {

        changeColor(color, port_);
        _delay_ms(200);
        changeColor(Color::NONE, port_);
        _delay_ms(200);
    }
}
