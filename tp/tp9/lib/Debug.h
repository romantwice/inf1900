//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Implementation de la commande make debug.

#include <Uart.h>
#include <LEDControl.h>


// Mode debug
#ifdef DEBUG
#define DEBUG_PRINT(x) uart.print(x);

// Mode release
#else
#define DEBUG_PRINT(x) \
    do                 \
    {                  \
    } while (0);
#endif
