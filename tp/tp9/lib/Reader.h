//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Definition de la classe Reader.

#ifndef READER_H
#define READER_H

#include <Interpreteur.h>
#include <memory_24.h>
#include <Uart.h>

class Reader
{
public:
    Reader();
    ~Reader();
    void readandInstruct(Memoire24CXXX &memory, Interpreteur &interpreter, 
        Uart &uart, LEDControl &led, volatile uint8_t &portLed, 
        MotorPWM &motor, volatile uint8_t &portMotor, Piezo &piezo);
    void readData(uint8_t position, uint8_t *dataRead, 
        Memoire24CXXX &memory, Uart &uart);
    uint16_t getByteCodeSize(Memoire24CXXX &memory, Uart &uart);
    void writeByteCodeInMemory(Memoire24CXXX &memory, Uart &uart);
    void writeByteCodeSize(Memoire24CXXX &memory, Uart &uart);
    void readAllMemory(Memoire24CXXX &memory, Uart &uart);
};
#endif //! READER_H