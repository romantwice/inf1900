//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Implementation de la classe Reader.

#include <Reader.h>
Reader::Reader()
{
}

Reader::~Reader()
{
}

void Reader::readandInstruct(Memoire24CXXX &memory, Interpreteur &interpreter,
                             Uart &uart, LEDControl &led, volatile uint8_t &portLed,
                             MotorPWM &motor, volatile uint8_t &portMotor, Piezo &piezo)

{  
    uint16_t size = getByteCodeSize(memory, uart);
    //uart.uartTransmition(size);

    uint16_t positionToRemember; // pour la boucle for c'est la position de l'instruction qui suit instruction for
    int loopCounter = 0;
    bool loopInitialized = true;     // false quand on rentre la premiere fois dans boucle apres true pour initialization
    uint8_t instruction = 0xFF;
    uint8_t operand = 0;
   
    uint16_t i = 0x0002;

    while (i < size)
    {
        uint8_t dataRead;
        readData(i, &dataRead, memory, uart);
        uart.uartTransmition(dataRead);

        if (i % 2 == 0) 
        {
            instruction = dataRead;
            if (instruction == 0xFF)
            {
                // End of program
                interpreter.instructionInterpreteur(instruction, operand, led, portLed, motor, portMotor, piezo);
                break;
            }
            else if (instruction == 0xC0)
            {
                // Loop start
                positionToRemember = i;
                loopInitialized = false;
            }
            else if (instruction == 0xC1)
            {
                // Loop end
                if (loopCounter == 0)
                { // looped operand + 1 times
                    goto exit_loop;
                }
                else
                {
                    i = positionToRemember; // puis i++ -> 1ere inst apres debut boucle
                    loopCounter--;
                }
            }
        }
        else
        {
            if (loopInitialized == false)
            {
                loopCounter = dataRead; // initialises the number of loops to the operand
                loopInitialized = true;
            }
            operand = dataRead;
            interpreter.instructionInterpreteur(instruction, operand, led, portLed, motor, portMotor, piezo);
        }
    exit_loop:;
        i++;
    }
}

void Reader::readData(uint8_t position, uint8_t *dataRead, Memoire24CXXX &memory, Uart &uart)
{
    memory.lecture(position, dataRead);
}

void Reader::writeByteCodeSize(Memoire24CXXX &memory, Uart &uart)
{
    uint8_t dataRead1;
    uint8_t dataRead2;
    dataRead1 = uart.uartReceive();
    memory.ecriture(0, dataRead1);
    dataRead2 = uart.uartReceive();
    memory.ecriture(1, dataRead2);
}

// Read the first two bytes to get size of the file, a 16 bits value
uint16_t Reader::getByteCodeSize(Memoire24CXXX &memory, Uart &uart) {
    uint8_t dataRead1;
    uint8_t dataRead2;
    memory.lecture(0, &dataRead1);
    uart.uartTransmition(dataRead1);
    memory.lecture(1, &dataRead2);
    uart.uartTransmition(dataRead2);
    uint16_t result = dataRead1; 
    result = result << 8;
    result += dataRead2;
    return result;
}

void Reader::writeByteCodeInMemory(Memoire24CXXX &memory, Uart &uart)
{
    const int WRITING_DELAY = 5000;

    writeByteCodeSize(memory, uart);
    const uint16_t size = getByteCodeSize(memory, uart);
    uint8_t test;

    for (uint16_t i = 2; i < (size + 2); i++)
    {
        test = uart.uartReceive();
        memory.ecriture(i, test);
    }
    _delay_ms(WRITING_DELAY);
}

// To only use to read from memory in RS-232 transmission to the pc.
void Reader::readAllMemory(Memoire24CXXX &memory, Uart &uart)
{
    for (uint16_t i = 0x0000; i < 0xFFFF; i++)
    {
        uint8_t dataRead;
        memory.lecture(i, &dataRead);
        uart.uartTransmition(dataRead);
    }
}
