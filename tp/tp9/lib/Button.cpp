//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Implementation de la classe button.

#ifndef F_CPU
#define F_CPU 8000000UL
#endif

#include "Button.h"

bool Button::didPress(volatile uint8_t pinToCheck, uint8_t mask) const
{
    bool didAlreadyPress = false;

    if (pinToCheck & mask)
        didAlreadyPress = true;

    _delay_ms(debouncingDelay_);
    return (pinToCheck & mask) && didAlreadyPress;
}