//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Implementation de la classe timer1CTC.

#include "Timer1CTC.h"

Timer1CTC::Timer1CTC()
{
	initRegister();
}

Timer1CTC::~Timer1CTC()
{
}

void Timer1CTC::initRegister()
{
	// Initialize timer
	TCNT1 = 0;

	// Set TCCR1A on compare match to high level (page 128)
	TCCR1A |= (1 << COM1A1) | (1 << COM1A0) | (1 << COM1B1) | (1 << COM1B0);

	// Set the CTC Mode (page 119)
	TCCR1A &= ~(1 << WGM11) & ~(1 << WGM10);
	TCCR1B |= (1 << WGM12);

	TCCR1C = 0;
	TIMSK1 |= (1 << OCIE1A);
}

void Timer1CTC::startTimer(const uint16_t outputCompareValue)
{
	// Set 1024 prescaler
	TCCR1B |= (1 << WGM12) | (1 << CS12) | (1 << CS10);
	TCCR1B &= ~(1 << CS11);
	OCR1A = outputCompareValue;
}

void Timer1CTC::setTimerBySeconds(const int seconds)
{
	startTimer(cyclesBySecond_ * seconds);
}

void Timer1CTC::stopTimer()
{
	TCCR1B &= ~((1 << WGM12) | (1 << CS12) | (1 << CS10));
}