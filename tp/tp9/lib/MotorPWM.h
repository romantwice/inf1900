//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Implementation de la classe motorPWM.

/*
Timer0 in PWM phase correct, fix TOP value to 0xff. Set OC0X on Compare Match when up-counting.
Clear OC0X on Compare Match when down-counting .
OC0A = PB3 - Left motor
OC0B = PB4 - Right motor
PB2  = direction signal left motor
PB5  = direction signal right motor
 */

#ifndef MOTORPWM_H
#define MOTORPWM_H

#include <avr/io.h>
#include "Timer1CTC.h"
#include <util/delay.h>

class MotorPWM
{
public:
	/*  MotorPWM::MotorPWM(int leftMotorPinDirection, int rightMotorPinDirection)
		 Constructor : Proceed to initialization
	In parameter  : int leftMotorPinDirection, int rightMotorPinDirection - Output signal pin to send direction signal to motor
	Out parameter : MotorPWM object */
	MotorPWM(const int leftMotorPinDirection, const int rightMotorPinDirection);

	/*  MotorPWM::~MotorPWM()
		Destructor : Do nothing */
	~MotorPWM();

	/*  void MotorPWM::initRegister()
		 Initialize the register of the timer0 in PWM phase correct, TOP 0xFF mode. Called
		 by the constructor.
	TCCR0A : COM0A0, COM0A1, COM0B0, COM0B1 - Set OC0B on Compare Match when up-counting. Clear OC0B on
	Compare Match when down-counting (Page 102)
				WGM00, WGM01 - Mode 1 (Page 103)
	TCCR0B : CS00, CS01, CS02 - Prescaler at clk/8 (Page 104)
				WGM02 - Mode 1 (Page 103) */
	void initRegister();

	/*  void MotorPWM::ajustPWM(uint8_t percentA, uint8_t percentB)

	In parameter  : uint8_t percentMotorLeft  - Value between 0 and 255 - 0 is highest speed.
												Value up to 0xA0 is not enought to move the wheel
	Out parameter : uint8_t percentMotorRight - Value between 0 and 255 - 0 is highest speed.
												Value up to 0xA0 is not enought to move the wheel*/
	void ajustSpeed(const uint8_t percentMotorLeft, const uint8_t percentMotorRight);

	/* Set the direction port for each motor to backward or forward mode
   In parameter  : volatile uint8_t& port
   Out parameter : none */
	void leftMotorRunForward(volatile uint8_t &port);
	void leftMotorRunBackward(volatile uint8_t &port);
	void rightMotorRunForward(volatile uint8_t &port);
	void rightMotorRunBackward(volatile uint8_t &port);

	void runStraightForward(const uint8_t speed, volatile uint8_t &port);
	void runStraightBackward(const uint8_t speed, volatile uint8_t &port);
	void turn90degreeRight(volatile uint8_t &port);
	void turn90degreeLeft(volatile uint8_t &port);
	void stopMotor(volatile uint8_t &port);

private:
	int leftMotorPinDirection_;
	int rightMotorPinDirection_;
};

#endif // !MOTORPWM_H
