//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Definition de la classe timer1CTC.

#ifndef TIMER1CTC_H
#define TIMER1CTC_H

#include <avr/io.h>

/*
Timer1 in CTC mode with 1024 prescaler and interrupt
after output compare value.
 */

class Timer1CTC
{
public:
	/*  Timer1CTC::Timer1CTC()
		Constructor : Proceed to initialization
	In parameter  : none
	Out parameter : none
	Don't forget to enable interruptions with sei() */
	Timer1CTC();

	/*  Timer1CTC::~Timer1CTC()
		Destructor : Do nothing
	In parameter  : none
	Out parameter : none */
	~Timer1CTC();

	/*  void Timer1CTC::initRegister()
		Initialize the register (need to be precise) of the timer1 in CTC mode. Called
		by the constructor and can be call to put TCNT1 register to 0.
	In parameter  : none
	Out parameter : none */
	void initRegister();

	/*  void Timer1CTC::startTimer(const uint16_t outputCompareValue)
		Start the timer with the interrupt enable at the ouputCompareValue
	In parameter  : uint16_t outputCompareValue - Value that send a interrupt signal - 1 sec = 7812
	Out parameter : none */
	void startTimer(const uint16_t outputCompareValue);

	// Timer interruption (TIMER1_COMPA_vect) must be declared.
	// This function throws the interruption in x seconds (parameter)
	void setTimerBySeconds(const int seconds);

	/*  void Timer1CTC::stopTimer()
		  Stop the timer at it state.
	In parameter  : none
	Out parameter : none */
	void stopTimer();

private:
	static const uint16_t cyclesBySecond_ = 7812;
};

#endif // !TIMER1CTC_H
