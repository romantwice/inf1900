// /*	In/out pin configuration table
// +-------- + -------------- - +------------------------------------ - +
// | Broche | Entree / Sortie | Description							 |
// +-------- + -------------- - +------------------------------------ - +
// | PA0	 | Sortie		   | Led1									 |
// | PA1	 | Sortie		   | Led1									 |
// | PA2	 | Entree		   | Capteur de distance					 |
// | PB2	 | Sortie		   | Signal direction roue gauche			 |
// | PB3	 | Sortie		   | Signal PWM roue gauche					 |
// | PB4	 | Sortie		   | Signal PWM roue droite					 |
// | PB5	 | Sortie		   | Signal direction roue droite			 |
// | PC2	 | Entree		   | Suiveur de ligne O1(Gauche)			 |
// | PC3	 | Entree		   | Suiveur de ligne O2					 |
// | PC4	 | Entree		   | Suiveur de ligne O3(Centre)			 |
// | PC5	 | Entree		   | Suiveur de ligne O4					 |
// | PC6	 | Entree		   | Suiveur de ligne O5(Droite)			 |
// | PC7	 | Sortie		   | Signal calibration suiveur de ligne	 |
// | PD2    | Entree          | Bouton poussoire interrupt sur la carte |
// | PD3	 | Entree		   | Bouton poussoire externe    			 |
// | PD6	 | Sortie		   | Ground Piezo							 |
// | PD7	 | Sortie		   | Signal PWM Piezo						 |
// +------- + --------------- + --------------------------------------- +
// */

// #include <avr/io.h>
// #include <avr/interrupt.h>
// #include "DrivingController.h"
// #include "MissionSelector.h"
// #include "ObstacleDetector.h"
// #include "LEDControl.h"
// #include <util/delay.h>

// #define F_CPU 8000000UL
// #define GLOBAL_CONST_SPEED 0x80

// // TEST OBSTACLE DETECTOR 
// void testRoman()
// {
//     led.changeColor(Color::NONE);
//     Uart uart_ = Uart(Mode::RECEIVEANDTRANSMIT);
//     ObstacleDetector obstacleDetector_ = ObstacleDetector();
//     while (true)
//     {
//         obstacleDetector_.checkForObstacle();
//         uart_.uartTransmition(obstacleDetector_.getNumObstacles());
//         if (obstacleDetector_.getNumObstacles() == 3)
//         {
//             obstacleDetector_.stockObstacleList();
//             led.changeColor(Color::GREEN);
//             // break;
//         }
//     }

//     Button externalButton(ButtonType::EXTERNAL);
//     while (!externalButton.didRelease())
//     {
//     }

//     Reader reader = Reader();
//     reader.readAllMemory();

//     Memoire24CXXX memory_ = Memoire24CXXX();
//     Uart uart_ = Uart(Mode::RECEIVEANDTRANSMIT);
//     for (uint16_t i = 0x0000; i < 0xFFFF; i++)
//     {
//         memory_.ecriture(i, 'r');
//         uart_.uartTransmition('r');
//     }
// }

// void testBase()
// {
//     uint16_t value = converter.lecture(PA2);
//     value = value >> 2;
//     while (true)
//     {
//         if (obs.getDistance() > 15 && obs.getDistance() < 30)
//         {
//             // locationOfObstacles_[0] = 'N';
//             obs.startObstacleDetectedEvent();
//         }
//         else if (obs.getDistance() > 40 && obs.getDistance() < 60)
//         {
//             // locationOfObstacles_[0] = 'O';
//             obs.startObstacleDetectedEvent();
//         }
//     }
// }

// int f()
// {
//     DDRA |= (1 << PA0) | (1 << PA1);
//     DDRA &= ~(1 << PA2);
//     DDRD &= ~(1 << PD7);
//     // LEDControl led = LEDControl();
//     can converter = can();
//     Piezo piezo = Piezo();
//     ObstacleDetector obs = ObstacleDetector();
//     LEDControl led = LEDControl();

//     while (true)
//     {
//         if (obs.isObstacleDetected())
//         {
//             obs.getObstacleInfo();
//             piezo.startSound(67);
//         }
//     }

//     return 0;
// }