//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Definition de la classe qui detectera les obstacles.

#pragma once
#include <avr/io.h>
#include <EnumPoint.h>
#include <can.h>
#include <memory_24.h>
#include <Piezo.h>
#include <Debug.h>
#include <LEDControl.h>

enum class DetectionState
{
    CLOSE,
    FAR,
    NO_DETECTION
};

class ObstacleDetector
{
public:
        ObstacleDetector();

        // Nouvelle fonctions:
        uint8_t detect(void);
        bool atLeastOneCloseDetection(void);
        void cleanReadings(void);
        uint8_t computeDetectionAverage(void);
        bool checkForObstacle2(void);
        bool isObstacle2(void) const;
        void addReading(void);
        bool isNear2(uint8_t detection) const;
        bool isFar2(uint8_t detection) const;
        void setLastDetection(DetectionState state);

        uint16_t getCanValue(void);
        void determineCloseOrFar(void);
        void startObstacleDetectedEvent(void);
        void determineObstacleToStock(void);
        bool checkForObstacle(void);
        void getObstacleInfo(void);
        void stockObstacleList(void);
        uint8_t* retrieveObstacleList(uint8_t* list);
        bool isObstacleDetected(void);
        void verifySwapForSpecialCase(void);
        bool isThereThreeObstacles(void);
        bool isObstacle(void) const;
        bool isNear(void) const;
        bool isFar(void) const;
        void setAverage(void);
        // boolean getIsDetecting(void);
        void stockObstacle(int i);

    private:
        int numObstacles_ = 0;  // Incremented when we detect an obstacle
        const static int obstaclesSize_ = 3;
        uint16_t locationOfObstacles_[obstaclesSize_] = {};
        uint16_t canValue_;
        // Changer appels de ces deux la par bon noms
        bool isClose_ = false; 
        bool isFar_ = false;

        bool isObstacle_ = false;

        bool isDetecting_ = false;
        const static uint8_t maxReadings_ = 7;
        uint8_t readings_[maxReadings_] = {}; 
        uint8_t readingCounter_ = 0;
        bool isDeterminingObstaclesFinished = false;

        DetectionState lastDetection_ = DetectionState::NO_DETECTION;

        // TODO: un jour effacer ancien can values (le plus tard possible)
        // const uint8_t nearObstacleMax_ = 77;
        // const uint8_t nearObstacleMin_ = 60;
        // const uint8_t farObstacleMax_ = 33;
        // const uint8_t farObstacleMin_ = 27;


        // const uint8_t nearObstacleMax_ = 77;
        // const uint8_t nearObstacleMin_ = 43;
        // const uint8_t farObstacleMax_ = 24;
        // const uint8_t farObstacleMin_ = 17;
        
        // POWER SUPPLY VALEURS 
        // const uint8_t nearObstacleMax_ = 110;
        // const uint8_t nearObstacleMin_ = 43; // TODO: peut-etre decendre un peu     
        // const uint8_t farObstacleMax_ = 24;
        // const uint8_t farObstacleMin_ = 14;
        
        // INTERVALLES AVEC BATTERIES FULL
        // const uint8_t nearObstacleMax_ = 110;
        // const uint8_t nearObstacleMin_ = 34;      
        // const uint8_t farObstacleMax_ = 24;
        // const uint8_t farObstacleMin_ = 12;


        // INTERVALLES AVEC POWER SUPPLY ET AREF = 3.20
        const uint8_t nearObstacleMax_ = 102;
        const uint8_t nearObstacleMin_ = 65;      
        const uint8_t farObstacleMax_ = 27;
        const uint8_t farObstacleMin_ = 23;
        // const uint8_t farObstacleMin_ = 17;

        uint8_t averageCanValue_ = 0;

        can converter_ = can();
        Memoire24CXXX memory_ = Memoire24CXXX();
        Piezo piezo_ = Piezo();
        LEDControl led_ = LEDControl();
};