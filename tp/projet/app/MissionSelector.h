//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Definition de la classe qui executera les mission selectionnees.

#ifndef MISSIONSELECTOR_H
#define MISSIONSELECTOR_H


#include <LEDControl.h>
#include <EnumColor.h>
#include <EnumPoint.h>
#include <util/delay.h>
#include <Button.h>
#include "ObstacleDetector.h"
#include "DrivingController.h"
#include "FollowLineSensor.h"
#include "Timer1CTC.h"
#include "EnumSensorMode.h"

// TODO: Bouger cette struct vers le fichier Mission.h,
// il faut probablement modifier le makefile pour que l'import fonctionne ensuite
struct Mission
{
    Point starterPoint;
    Color color;
};


enum class MissionState
{
    START,
    DRIVING,
    STOP
};

class MissionSelector
{
public:
    MissionSelector();
    ~MissionSelector();

    void next();
    void confirm();

    void executeMissionA();
    void executeMissionB();
    void executeMissionS();
    void drive();
    void startMission();
    void startSelector();
    void nextPoint();
    void startTimer(const int nSeconds);
    void stopTimer();
    void detectedObjectSequence();
    // uint8_t readPoint(Point point);

private:
    Mission missions_[3];
    Mission selectedMission_;
    MissionState missionState_ = MissionState::START;
    Point currentPoint_;
    DrivingController drivingController_;
    ObstacleDetector obstacleDetector_ = ObstacleDetector();
    FollowLineSensor lineSensor_ = FollowLineSensor();
    uint8_t indexSelectedMission_ = 0;
    LEDControl led_ = LEDControl();
    Timer1CTC timer_ = Timer1CTC();
    bool timerRunning_ = false;
    bool timerIsFinish_ = false;

    bool didCallObstacleDetection_ = false; 
    bool didDetect_ = false; 
    InstructionMsk instruction_;

    uint8_t listTest_[3]={73, 0,0};; //TODO : a enlever une fois que les choix de directions fonctionnent
};
#endif //! MISSIONSELECTOR_H