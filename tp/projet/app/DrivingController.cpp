#ifndef F_CPU
#define F_CPU 8000000UL
#endif
#include "DrivingController.h"

DrivingController::DrivingController()
{
}

DrivingController::~DrivingController()
{
}

void DrivingController::enableDrivingController(SensorMode sensorMode)
{
	enableDrivingController_ = true;
	enableInterruption();
	sensorMode_ = sensorMode;
}

void DrivingController::disableDrivingController()
{
	enableDrivingController_ = false;
}

void DrivingController::enableInterruption()
{
	PCICR |= (1 << PCIE2);
}

void DrivingController::disableInterruption()
{
	PCICR &= ~(1 << PCIE2);
}

void DrivingController::makeTurn(Direction direction, Angle angle)
{
	if (direction == Direction::RIGHT)
	{
		wheels_.leftWheelRunForward();
		wheels_.ajustSpeed(0x70, 0x00);
		delayLoop(static_cast<int>(angle));
		wheels_.stopWheel();
	}
	else if (direction == Direction::LEFT)
	{
		wheels_.rightWheelRunForward();
		wheels_.ajustSpeed(0x00, 0x69);
		delayLoop(static_cast<int>(angle));
		wheels_.stopWheel();
	}
}

void DrivingController::makeTurnReverse(Direction direction, Angle angle)
{
	if (direction == Direction::RIGHT)
	{
		wheels_.leftWheelRunBackward();
		wheels_.ajustSpeed(0xFF, 0x00);
		delayLoop(3);
		wheels_.ajustSpeed(0x70, 0x00);
		delayLoop(static_cast<int>(angle));
		wheels_.stopWheel();
	}
	else if (direction == Direction::LEFT)
	{
		wheels_.rightWheelRunBackward();
		wheels_.ajustSpeed(0x00, 0xFF);
		delayLoop(3);
		wheels_.ajustSpeed(0x00, 0x70);
		delayLoop(static_cast<int>(angle));
		wheels_.stopWheel();
	}
}

InstructionMsk DrivingController::followLine()
{
	if (enableDrivingController_)
	{
		InstructionMsk instruction = followLineSensor_.getInstruction();
		InstructionMsk instruction2;
		switch (instruction)
		{
		case InstructionMsk::ONLINE:
			inertiaBoost();
			wheels_.runStraightForward(0x60, 0x50);
			break;
		case InstructionMsk::ISLITTLELEFT:
			inertiaBoost();
			wheels_.ajustSpeed(0x67, 0x40);
			break;
		case InstructionMsk::ISLITTLERIGHT:
			inertiaBoost();
			wheels_.ajustSpeed(0x40, 0x57);
			break;
		case InstructionMsk::ISFARLEFT:
			// inertiaBoost();
			wheels_.ajustSpeed(0x79, 0x00);
			break;
		case InstructionMsk::ISFARRIGHT:
			// inertiaBoost();
			wheels_.ajustSpeed(0x00, 0x70);
			break;
		case InstructionMsk::IS90LEFT:
			makeTurn(Direction::LEFT, Angle::NINETY);
			break;
		case InstructionMsk::IS90RIGHT:
			makeTurn(Direction::RIGHT, Angle::NINETY);
			break;
		case InstructionMsk::ISDOUBLELINE:
			stop();
		case InstructionMsk::ISFULLLINE :
			delayLoop(1);
			instruction2 = followLineSensor_.getInstruction();
			if (instruction2 != InstructionMsk::ISFULLLINE) // 2ieme lecture pour confirmer que c<est une ligne pleine et non une fausse lecture
			{
				 instruction = instruction2;
			}
			break;
		case InstructionMsk::ISNOTHING :
			// stop();
			break;
		default:
			// stop();
			break;
		}
		return instruction;
	}
	return InstructionMsk::ISSTOP;
}

InstructionMsk DrivingController::runHallway()
{
	InstructionMsk instruction = followLineSensor_.getHallwayInstruction();
	switch (instruction)
	{
	case InstructionMsk::ISFARLEFT:
		wheels_.ajustSpeed(0x79, 0x00);
		break;
	case InstructionMsk::ISFARRIGHT:
		wheels_.ajustSpeed(0x00, 0x70);
		break;
	default:
		break;
	}
	return instruction;
}

void DrivingController::stopHallway()
{
	if(lastInstruction_ == InstructionMsk::ISFARRIGHT) { wheels_.ajustSpeed(0x70, 0x00); } // Essayer sans la condition
	bool sensorSetter[5] = {0, 0, 0, 0, 1};
	followLineSensor_.setLastRead(sensorSetter);
}

void DrivingController::stop()
{
	wheels_.stopWheel();
	// TODO: debugger cette fonction, ne stop pas dans la missionA si disableInterruption() nest pas appelee
	enableDrivingController_ = false;  
	disableInterruption();
}

void DrivingController::park() // Hardcode for the final parking
{
	delayLoop(80);
	wheels_.runStraightBackward(0xFF, 0xFF);
	delayLoop(2);
	wheels_.runStraightBackward(0x79, 0x70);
	delayLoop(70);
	wheels_.stopWheel();
	delayLoop(40);
	makeTurnReverse(Direction::RIGHT, Angle::NINETY);
	delayLoop(40);
	wheels_.runStraightBackward(0xFF, 0xFF);
	delayLoop(2);
	wheels_.runStraightBackward(0x79, 0x70);
	delayLoop(97);
	wheels_.stopWheel();
}

void DrivingController::reverse()
{
	wheels_.runStraightBackward(0x70, 0x60);
	delayLoop(50); // Need test for a 1.5 inch travel
	wheels_.stopWheel();
}

void DrivingController::delayLoop(int n25Ms)
{
	for (int i = 0; i < n25Ms; i++)
	{
		_delay_ms(25);
	}
}

void DrivingController::inertiaBoost()
{
	wheels_.runStraightForward(0xAF, 0xAF);
	delayLoop(1);
}

void DrivingController::detectedObstacleSequence(Direction direction)
{
	makeTurn(direction, Angle::FIFTEEN);
	delayLoop(40);
	makeTurnReverse(direction, Angle::FIFTEEN);
	delayLoop(20);
	wheels_.runStraightForward(0x00, 0x00); // Ajust the direction to forward
	stop();
}

// Determines behavior of robot in the middle section of mission B.
//	A UTILISER AVEC ISTHERETHREEOBSTACLES() -> EXAMPLE PINNED MESSAGES DISCORD
void DrivingController::determineDecisionInMissionB(uint8_t i)
{
	// Using switch simplifies lecture -> no need for order to be remembered or played with ;)
	uint8_t obstacleList[3];
	ObstacleDetector_.retrieveObstacleList(obstacleList); // TODO: Assurer que l'appel de cette methode est correcte

	switch (obstacleList[i])
	{
	case static_cast<uint8_t>(Point::O):
		makeTurn(Direction::LEFT, Angle::FIFTEEN);
		break;

	case static_cast<uint8_t>(Point::N):
		makeTurn(Direction::RIGHT, Angle::FIFTEEN);
		break;

	case static_cast<uint8_t>(Point::L):
		detectedObstacleSequence(Direction::RIGHT);
		break;

	case static_cast<uint8_t>(Point::K):
		detectedObstacleSequence(Direction::LEFT);
		break;

	case static_cast<uint8_t>(Point::IG):
		makeTurn(Direction::RIGHT, Angle::FIFTEEN);
		break;

	case static_cast<uint8_t>(Point::HF):
		makeTurn(Direction::LEFT, Angle::FIFTEEN);
		break;

	default:
		break;
	}
}

void DrivingController::stuck()
{
	wheels_.runStraightForward(0x80, 0x70);
}

SensorMode DrivingController::getSensorMode() const
{
	return sensorMode_;
}

void DrivingController::setSensorMode(SensorMode sensorMode)
{
	sensorMode_ = sensorMode;
}

void DrivingController::turnRight()
{
	wheels_.runStraightForward(0x70, 0x00);
}