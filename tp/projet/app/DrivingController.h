#pragma once
#include "Wheel.h"
#include <avr/io.h>
#include "EnumDirection.h"
#include "EnumAngle.h"
#include "EnumInstructionMsk.h"
#include <util/delay.h>
#include "FollowLineSensor.h"
#include <util/delay.h>
#include "ObstacleDetector.h"
#include "EnumSensorMode.h"

#ifndef DRIVINGCONTROLLER_H
#define DRIVINGCONTROLLER_H

class DrivingController
{
public:
	DrivingController();
	~DrivingController();

	void enableInterruption();
	void disableInterruption();
	void enableDrivingController(SensorMode sensorMode);
	void disableDrivingController();
	void makeTurn(Direction direction, Angle angle);
	void makeTurnReverse(Direction direction, Angle angle);
	InstructionMsk followLine();
	void stopHallway();
	void stop();
	void park();
	void reverse();
	InstructionMsk runHallway();
	void detectedObstacleSequence(Direction direction);
	void inertiaBoost();
	void determineDecisionInMissionB(uint8_t i);
	void stuck();
	void turnRight();
	SensorMode getSensorMode() const;
	void setSensorMode(SensorMode sensorMode);

	void delayLoop(int n25Ms);

private:
	Wheel wheels_ = Wheel(PB2, PB5);
	FollowLineSensor followLineSensor_ = FollowLineSensor();
	bool enableDrivingController_ = true;
	ObstacleDetector ObstacleDetector_ = ObstacleDetector();
	InstructionMsk lastInstruction_ = InstructionMsk::ISNOTHING;
	SensorMode sensorMode_;
};

#endif //DRIVINGCONTROLLER_H