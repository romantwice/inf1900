/*	In/out pin configuration table
+-------- + -------------- - +------------------------------------ - +
| Broche | Entree / Sortie | Description							 |
+-------- + -------------- - +------------------------------------ - +
| PA0	 | Sortie		   | Led1									 |
| PA1	 | Sortie		   | Led1									 |
| PA2	 | Entree		   | Capteur de distance					 |
| PB2	 | Sortie		   | Signal direction roue gauche			 |
| PB3	 | Sortie		   | Signal PWM roue gauche					 |
| PB4	 | Sortie		   | Signal PWM roue droite					 |
| PB5	 | Sortie		   | Signal direction roue droite			 |
| PC2	 | Entree		   | Suiveur de ligne O1(Gauche)			 |
| PC3	 | Entree		   | Suiveur de ligne O2					 |
| PC4	 | Entree		   | Suiveur de ligne O3(Centre)			 |
| PC5	 | Entree		   | Suiveur de ligne O4					 |
| PC6	 | Entree		   | Suiveur de ligne O5(Droite)			 |
| PC7	 | Sortie		   | Signal calibration suiveur de ligne	 |
| PD2    | Entree          | Bouton poussoire interrupt sur la carte |
| PD3	 | Entree		   | Bouton poussoire externe    			 |
| PD6	 | Sortie		   | Ground Piezo							 |
| PD7	 | Sortie		   | Signal PWM Piezo						 |
+------- + --------------- + --------------------------------------- +
*/

#ifndef F_CPU
#define F_CPU 8000000UL
#endif

#include <avr/io.h>
#include <avr/interrupt.h>
#include "DrivingController.h"
#include "MissionSelector.h"
#include "ObstacleDetector.h"
#include "LEDControl.h"
#include <util/delay.h>
#include <Button.h>

// TODO: includes to remove
#include "Reader.h"
#include "memory_24.h"

#define F_CPU 8000000UL
#define GLOBAL_CONST_SPEED 0x80

LEDControl led = LEDControl();
MissionSelector missionSelector = MissionSelector();

ISR(PCINT2_vect)
{
	missionSelector.drive();		
	EIFR |= (1 << PCIF2);
}

ISR(TIMER1_COMPA_vect)
{
	// missionSelector.nextPoint();
	missionSelector.stopTimer();
}

volatile uint8_t gIsbuttonPressed = 0;
volatile uint8_t gIsInteruptPressed = 0;

void initializePort()
{
	DDRA |= (1 << PA0) | (1 << PA1);
	DDRA &= ~(1 << PA2);
	DDRB |= ((1 << PB2) | (1 << PB3) | (1 << PB4) | (1 << PB5));
	DDRC |= (1 << PC7);
	DDRC &= ~((1 << PC2) | (1 << PC3) | (1 << PC4) | (1 << PC5) | (1 << PC6));
	DDRD |= (1 << PD6) | (1 << PD7);
	DDRD &= ~(1 << PD2);
	DDRD &= ~(1 << PD3);
}

void initializeRegister()
{
	// Interrupt register
	cli();

	PCICR |= (1 << PCIE2);
	// drivingController.disableInterruption();
	PCMSK2 |= (1 << PCINT18) | (1 << PCINT19) | (1 << PCINT20) | (1 << PCINT21) | (1 << PCINT22);

	// CODE POUR INTERRUPTION SUR BUTTON
	// Bouton poussoire interne
	// EIMSK |= (1 << INT0);

	// EICRA |= (1 << ISC00);
	// EICRA &= ~(1 << ISC01);
	// // Bouton poussoire externe
	// EIMSK |= (1 << INT1);
	// EICRA |= (1 << ISC10);
	// EICRA &= ~(1 << ISC11);

	sei();
}

// Interruption sur bouton poussoir (POUR TEST)
ISR(INT1_vect)
{
	// missionSelector.next();
	// // anti-rebond
	// bool x0 = PORTD & (1 << PIND3);
	// _delay_ms(30);
	// bool x1 = PORTD & (1 << PIND3);

	// if (x1 != x0)
	// {
	//     return;
	// }
	// gIsbuttonPressed=!gIsbuttonPressed;
}

ISR(INT0_vect)
{
	EIMSK &= ~((1 << INT0) | (1 << INT1));
}

int main()
{
	initializePort();
	initializeRegister();
	missionSelector.startSelector();

	// // TEST LECTURE LISTE D'OBSTACLES
	// Reader reader = Reader();
	// reader.readAllMemory();

	// WIPE MEMORY
	// Memoire24CXXX memory = Memoire24CXXX();
	// for (uint16_t i = 0x0000; i < 0xFFFF; i++)
	// {
	// 	uint8_t ass = ' ';
	// 	memory.ecriture(i, 0);
	// 	_delay_ms(10);
	// }

	// TEST OBSTACLE DETECTOR
	// Uart uart_ = Uart(Mode::RECEIVEANDTRANSMIT) ;
	// ObstacleDetector obstacleDetector_ = ObstacleDetector();
	// while (true)
	// {
	// 	obstacleDetector_.detect();
	// 	// bool isObstacle = obstacleDetector_.checkForObstacle();
	// 	// if (isObstacle) {
	// 	// 	uart_.uartTransmition(obstacleDetector_.getNumObstacles());

	// 	// 	_delay_ms(2000);
	// 	// }
	// 	// if (obstacleDetector_.getNumObstacles() == 3) {
	// 	// 	obstacleDetector_.stockObstacleList();
	// 	// 	led.changeColor(Color::GREEN);
	// 	// 	// break;
	// 	// }
	// }

	// Button externalButton(ButtonType::EXTERNAL);
	// while (!externalButton.didRelease()) {}
}