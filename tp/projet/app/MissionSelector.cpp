//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Implementation de la classe qui executera les mission selectionnees.

#include <MissionSelector.h>
// #include "Uart.h"

// Uart uart_ = Uart(Mode::RECEIVEANDTRANSMIT);

MissionSelector::MissionSelector()
{
    Mission missionA = {Point::A, Color::GREEN};
    Mission missionB = {Point::B, Color::RED};
    Mission missionS = {Point::S, Color::AMBER}; // Ceci bloque faire avec interruptions tp4
    missions_[0] = missionA;
    missions_[1] = missionB;
    missions_[2] = missionS;
    // drivingController_.disableDrivingController();
    selectedMission_ = missions_[0];
}

MissionSelector::~MissionSelector()
{
}



void MissionSelector::detectedObjectSequence()
{
    // TODO: Ce serait bien que le delais pour la detection se fasse dans obstacleDetector mais
    // pas sur si une bonne idee vu qu'il faut arreter les moteurs au meme moment
    drivingController_.stop();
    drivingController_.disableInterruption();

    // This delay make sure that we don't detect the same obstacle more than one time
    _delay_ms(1000); // TODO: Remplacer chiffre magique
    // TODO: Faire relecture pour augmenter la precision? si oui, dans ObstacleDetector?
    obstacleDetector_.determineObstacleToStock();
    drivingController_.enableDrivingController(SensorMode::FOLLOWLINE);
    drivingController_.enableInterruption();
    drivingController_.stuck();
    _delay_ms(1500);
}

void MissionSelector::drive()
{
    SensorMode sensorMode = drivingController_.getSensorMode();
    switch (sensorMode)
    {
    case SensorMode::FOLLOWLINE :
        instruction_ = drivingController_.followLine();
        break;
    case SensorMode::HALLWAY :
        instruction_ = drivingController_.runHallway();
        break;
    default:
        break;
    }
}
// TODO:

void MissionSelector::executeMissionA()
{
    uint8_t detection = 0;
    drivingController_.enableDrivingController(SensorMode::FOLLOWLINE);

    switch (missionState_)
    {
        case MissionState::START:
        {
            // TODO: une meilleure facon de faire ca, ce serait d'appeler
            // wheels_.runStraightForward directement dans la boucle,
            // mais cette classe n'a pas d'acces sur wheels actuellement

            // if (instruction_ == InstructionMsk::ISFULLLINE)
            drivingController_.stuck();
    
            missionState_ = MissionState::DRIVING;
            break;
        }
        case MissionState::DRIVING:
        {
                // TODO: change par getInstruction de FollowLineSensor
                if (instruction_ == InstructionMsk::ISFULLLINE) {
                    missionState_ = MissionState::STOP;
                    break;
                }

                detection = obstacleDetector_.detect();
                if (obstacleDetector_.isFar2(detection)) 
                {
                    _delay_ms(45); // Debounce
                    detection = obstacleDetector_.detect();
                    if (obstacleDetector_.isNear2(detection)) {
                        led_.changeColor(Color::RED);
                        // uart_.uartTransmition(detection);
                        detectedObjectSequence();
                    } else if (obstacleDetector_.isFar2(detection)) {
                        led_.changeColor(Color::GREEN); 
                        // uart_.uartTransmition(detection);
                        detectedObjectSequence();
                    }
                }
                else if (obstacleDetector_.isNear2(detection)) 
                {
                    led_.changeColor(Color::RED);
                    // uart_.uartTransmition(detection);
                    detectedObjectSequence();
                }

                led_.changeColor(Color::NONE);

            break;
        }

        case MissionState::STOP:
        {

            drivingController_.stop();
            drivingController_.disableInterruption();
            obstacleDetector_.stockObstacleList();
            // TODO: debugger stockage de valeurs dans obstacleDetector_.stockObstacleList()
            // obstacleDetector_.stockObstacleList();

            selectedMission_.starterPoint = Point::END;
            break;
        }
        default:
            break;
    }


    // TODO: ameliorer precision de detection (jouer avec les intervalles ou la moyenne de detection)
    // ca se fait dans la classe ObstacleDetector

    // TODO: Logique pour regler probleme avec detection loin quand le poteau est proche:
    // des que isObstacle() est vrai, stocker les moyennes dans un tableau
    // si au moins une de ces moyennes est dans l'intervalle de poteau proche
    // alors c'est un poteau proche, sinon c'est poteau loin
}


void MissionSelector::executeMissionB()
{
     led_.changeColor(Color::RED); // juste pour tester 
    // drivingController_.followLine();
    // char obstacleList[] = {"I", "K", "L"};
    // uint8_t* obstacleList = obstacleDetector_.retrieveObstacleList();
    // bool middlePole = sizeof(obstacleList)==3;
    // InstructionMsk currentLineState;
    // bool hasTurn = false;

    switch (currentPoint_)
    {
    case Point::B:
        nextPoint();
        drivingController_.enableDrivingController(SensorMode::FOLLOWLINE);
        //         //peut etre pas une bonne methode puisqu'en se reajustant sur la ligne droite, il est islittleright parfois
        //         //Peut etre avec timer jusqu'au point E? A revoir avec les gars!

        //         // TODO: explique pourquoi on a besoin de la variable didSeePoint ou trouver un autre approche

        //         currentLineState = drivingController_.followLine();
        //         if (currentLineState == InstructionMsk::ISFARRIGHT || currentLineState == InstructionMsk::IS90RIGHT) {
        //             hasTurn = true;
        //         }
        //         if ((currentLineState == InstructionMsk::ISLITTLELEFT || currentLineState == InstructionMsk::ISLITTLERIGHT) && hasTurn)
        //         {
        //             led_.changeColor(Color::GREEN, PORTA);
        //             nextPoint();
        //             hasTurn = false;
        //         }
        //         // if (currentLineState == InstructionMsk::ISFARRIGHT || currentLineState == InstructionMsk::IS90RIGHT)

        //         // if(currentLineState == InstructionMsk::ISFARRIGHT)
        //         //     didSeePoint = true;
        //         // if (didSeePoint)
        //         //     if (currentLineState == InstructionMsk::ISFARRIGHT)
        //         //         nextPoint();
        //         //         didSeePoint = false;
        //         //     else {
        //         //         break;
        //         //     }
        //         break;

    case Point::C:
             led_.changeColor(Color::RED); // juste pour tester 
       nextPoint();
    //         currentLineState = drivingController_.followLine();
    //         if (currentLineState == InstructionMsk::ISFARRIGHT||currentLineState == InstructionMsk::IS90RIGHT) 
    //             hasTurn = true;
    //         if (currentLineState == InstructionMsk::ONLINE && hasTurn)
    //         {
    //             led_.changeColor(Color::NONE, PORTA);
    //             nextPoint();
    //             hasTurn = false;
    //         }
    //         break;
        
    case Point::D:
      nextPoint();
    //         led_.changeColor(Color::GREEN, PORTA);
    //         drivingController_.stop();
    //         break;

    case Point::E:
        if (drivingController_.followLine() == InstructionMsk::ISDOUBLELINE)
        {
            drivingController_.stop();
            drivingController_.delayLoop(20);
            if (listTest_[0] == static_cast<uint8_t>(Point::IG))
            {
                drivingController_.makeTurn(Direction::RIGHT, Angle::FIFTEEN); // TODO: metter une fonction pour cette partie
                drivingController_.delayLoop(10);
                drivingController_.enableDrivingController(SensorMode::FOLLOWLINE);
                drivingController_.followLine();
                currentPoint_ = Point::IG;
                startTimer(8); // TODO : verifier
                drivingController_.stop();
            }
            else
            {
                drivingController_.makeTurn(Direction::LEFT, Angle::FIFTEEN);
                drivingController_.delayLoop(10);
                drivingController_.enableDrivingController(SensorMode::FOLLOWLINE);
                drivingController_.followLine();
                currentPoint_ = Point::HF;
                startTimer(8); // TODO : verifier
                drivingController_.stop();
            }
        }
        // if(obstacleList[0]==static_cast<uint8_t>(Point::IG))
        // { drivingController_.makeTurn(Direction::RIGHT, Angle::FIFTEEN);
        // }else{
        //     drivingController_.makeTurn(Direction::LEFT, Angle::FIFTEEN);
        // }

        break;

    case Point::J:
        if (true)
        { // TODO : middlePole
            drivingController_.detectedObstacleSequence(Direction::RIGHT);
        }

        //         if(middlePole){
        //              startTimer(1);
        //             drivingController_.stop();
        //             if(obstacleList[0]==static_cast<uint8_t>(Point::L))
        //                 drivingController_.detectedObstacleSequence(Direction::RIGHT);
        //             else
        //                 drivingController_.detectedObstacleSequence(Direction::LEFT);
        //         }
        //         //encore un timer pour etre arrive au point M?
        //         nextPoint();

        //     case Point::M:
        //         drivingController_.followLine();
        //         if(obstacleList[0]==static_cast<uint8_t>(Point::O))
        //         {
        //             drivingController_.makeTurn(Direction::RIGHT, Angle::FIFTEEN);
        //         }else{
        //             drivingController_.makeTurn(Direction::LEFT, Angle::FIFTEEN);
        //         }
        //         nextPoint();
        //     break;
        // //P,Q,R peut etre pas necessaires
        //     case Point::P:

        //     break;

        //     case Point::Q:

        //     break;

        //     case Point::R:
        //         if(drivingController_.followLine()==InstructionMsk::ISFULLLINE){
        //             nextPoint();
        //         }
        //     break;

    default:
        break;
    }

    // for(auto obstacle :obstacleList){
    //     if(obstacle==)
    // }
    // lire les poteaux de la memoire
    // Avance
    // if (drivingController_.followLine() == InstructionMsk::ISDOUBLELINE)
    //	{ drivingController_.stop();
    //		reste des instructions} else on continue donc pas de else
    // choisir chemin
    // avancer
    // si poteau sur L ou K hoisir chemin et avancer
    // si pas de poteau :
    // reculer 1.5 pouces--> hardcode? Jeremie - Oui dans drivingController.reverse() ca va etre ca
    // drivingController_.stop();
    // delay_ms(1000);
    // avance
    // choisit chemin sans poteau
    // avance
}

void MissionSelector::executeMissionS()
{
    // led_.changeColor(Color::NONE, PORTA); // juste pour tester
    led_.changeColor(Color::NONE);
    switch (currentPoint_)
    {
    case Point::S:
        drivingController_.enableDrivingController(SensorMode::FOLLOWLINE); // Remets enable le controller apr's une ligne pleine du point S
        if (timerIsFinish_)
        { 
            nextPoint(); 
            led_.changeColor(Color::RED);
        } else
        {
            startTimer(4);
        }     
        if(instruction_ == InstructionMsk::ISFULLLINE)
        {
            drivingController_.stuck();
        }
        break;

    case Point::T:
        stopTimer();
        drivingController_.turnRight();
        drivingController_.enableDrivingController(SensorMode::HALLWAY);
        startTimer(8); // Nombre de seconde a tester
        nextPoint();
        break;

    // case Point::T1:
    //     if (timerIsFinish_) { nextPoint();}
    //     break;

    case Point::U:
        led_.changeColor(Color::RED);
        if (timerIsFinish_)
        { 
            nextPoint(); 
        } else
        {
            startTimer(4);
        } 
        break;

    case Point::V:
        stopTimer();
        drivingController_.stop();
        drivingController_.stopHallway();
        drivingController_.enableDrivingController(SensorMode::FOLLOWLINE);
        nextPoint();
        break;

    case Point::W:
        led_.changeColor(Color::GREEN);
        
        if(instruction_==InstructionMsk::ISNOTHING)
        { 
            nextPoint();
        }
        
        break;
    case Point::X:
        drivingController_.stop();
        drivingController_.park();
        selectedMission_.starterPoint = Point::END;       
    default:
        break;
    }
}

void MissionSelector::startMission()
{  
    bool didFinishMission = false;
    while (!didFinishMission) 
    {
        switch (selectedMission_.starterPoint)
        {
        case Point::A:
            executeMissionA();
            break;
        case Point::B:
            executeMissionB();
            break;
        case Point::S:
            executeMissionS();
            break;
        case Point::END:
            didFinishMission = true;
            break;
        default:
            break;
        }
    }
}

void MissionSelector::next()
{
    if (indexSelectedMission_ == 2)
    { // TODO: remplacer chiffre magique
        indexSelectedMission_ = 0;
        currentPoint_ = Point::A;
    }
    else
    {
        indexSelectedMission_++;
        currentPoint_ = missions_[indexSelectedMission_].starterPoint;
    }

    selectedMission_ = missions_[indexSelectedMission_];
}

void MissionSelector::confirm()
{
    led_.blink(Color::RED, 8);
    drivingController_.enableDrivingController(SensorMode::FOLLOWLINE);
    startMission();
}

void MissionSelector::startSelector()
{
    Button externalButton(ButtonType::EXTERNAL);
    Button internalButton(ButtonType::INTERNAL);
    drivingController_.disableDrivingController();
    while (!internalButton.didRelease())
    {
        led_.changeColor(selectedMission_.color);
        if (externalButton.didRelease())
            next();
    }
    confirm();
}

void MissionSelector::nextPoint()
{
    switch (currentPoint_)
    {
    case Point::A:
        currentPoint_ = Point::B;
        break;
    case Point::B:
        currentPoint_ = Point::C;
        break;
    case Point::C:
        currentPoint_ = Point::D;
        break;
    case Point::D:
        currentPoint_ = Point::E;
        break;
    case Point::E:
        break;
    case Point::HF:
        currentPoint_ = Point::J;
        break;
    case Point::IG:
        currentPoint_ = Point::J;
        break;
    case Point::J:
        currentPoint_ = Point::M;
        break;
    case Point::L:
        break;
    case Point::K:
        break;
    case Point::M:
        break;
    case Point::N:
        currentPoint_ = Point::P;
        break;
    case Point::O:
        currentPoint_ = Point::P;
        break;
    case Point::P:
        currentPoint_ = Point::Q;
        break;
    case Point::Q:
        currentPoint_ = Point::R;
        break;
    case Point::R:
        currentPoint_ = Point::S;
        break;
    case Point::S:
        currentPoint_ = Point::T;
        break;
    case Point::T:
        currentPoint_ = Point::U;
        break;
    case Point::U:
        currentPoint_ = Point::V;
        break;
    case Point::V:
        currentPoint_ = Point::W;
        break;
    case Point::W:
        currentPoint_ = Point::X;
        break;
    case Point::X:
        currentPoint_ = Point::Y;
        break;
        /* code */
        break;

    default:
        break;
    }
}

void MissionSelector::startTimer(const int nSeconds)
{
    if (!timerRunning_)
    {
        timerRunning_ = true;
        timer_.setTimerBySeconds(nSeconds);
    }
}

void MissionSelector::stopTimer()
{
    timerIsFinish_ = true;
    timerRunning_ = false;
    timer_.stopTimer();
}

// const char MissionSelector::readPoint(Point point){

//     switch(point){
//         case Point::IG:
//             return 'IG';
//             break;
//         case Point::L:
//             return 'L';
//             break;
//         case Point::O:
//             return 'O';
//             break;
//         default:
//             return ' ';
//     }
// }
