//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Implementation de la classe qui detectera les obstacles.
//  Formule pour CAN:
//      CAN = (255 * Voltage (V)) / Vref (V)
//  Ou Vref est egal a 5 V dans notre cas.
//  Et la valeur de CAN nous sera donne par la methode lecture(uint8_t pos)

//  Pourvu que la periode de lecture du capteur est de 38 +- 10 ms
//  plusieurs mesures sont prises pour en calculer une moyenne et viser une bonne precision.

#include <ObstacleDetector.h>
// #include "Uart.h"

Uart uart_ = Uart(Mode::RECEIVEANDTRANSMIT);

ObstacleDetector::ObstacleDetector()
{
}

// Returns average CAN value
uint16_t ObstacleDetector::getCanValue()
{
    uint8_t numVals = 5;
    int sum;
    for (int i = 0; i < numVals; i++)
    {
        canValue_ = converter_.lecture(PA2);
        canValue_ = canValue_ >> 2;
        sum += canValue_;
    }
    canValue_ = sum / numVals;
    return canValue_;
}

bool ObstacleDetector::isObstacleDetected()
{
    if (getCanValue() < nearObstacleMax_ && getCanValue() > farObstacleMin_)
    {
        isDetecting_ = true;
    }
    else
    {
        isDetecting_ = false;
    }
    return isDetecting_;
}

// Ces deux methodes etaient mon implementation (enlever a la fin)
void ObstacleDetector::determineCloseOrFar(void)
{
    if (getCanValue() < nearObstacleMax_ && getCanValue() > nearObstacleMin_)
    {
        isClose_ = true;
    }
    else if (getCanValue() < farObstacleMax_ && getCanValue() > farObstacleMin_)
    {
        isFar_ = true;
    }
}
void ObstacleDetector::getObstacleInfo(void)
{
    if (isObstacleDetected())
    {
        determineCloseOrFar();
        determineObstacleToStock();
        _delay_ms(3000);
    }
}

bool ObstacleDetector::isNear2(uint8_t detection) const
{
    return ((detection <= nearObstacleMax_) && (detection >= nearObstacleMin_));
}

bool ObstacleDetector::isFar2(uint8_t detection) const
{
    return ((detection <= farObstacleMax_) && (detection >= farObstacleMin_));
}

// TODO: A enlever peut etre!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
bool ObstacleDetector::isNear() const
{
    return (averageCanValue_ <= nearObstacleMax_ && averageCanValue_ >= nearObstacleMin_);
}
bool ObstacleDetector::isFar() const
{
    return (averageCanValue_ <= farObstacleMax_ && averageCanValue_ >= farObstacleMin_);
}

void ObstacleDetector::addReading()
{
    readings_[readingCounter_] = averageCanValue_;
    if (readingCounter_ < maxReadings_)
        readingCounter_++;
    else
        readingCounter_ = 0;
}

bool ObstacleDetector::isObstacle2() const
{
    return isObstacle_;
}

bool ObstacleDetector::isObstacle() const
{
    return (isFar() || isNear());
}

uint8_t ObstacleDetector::computeDetectionAverage()
{
    const uint8_t nValues = 15;
    int somme = 0;
    for (uint8_t i = 0; i < nValues; i++)
    {
        canValue_ = converter_.lecture(PA2);
        canValue_ = canValue_ >> 2;
        somme += canValue_;
    }

    averageCanValue_ = (somme / nValues);
    return somme / nValues;
}

uint8_t ObstacleDetector::detect()
{
    averageCanValue_ = computeDetectionAverage();
    isObstacle_ = (isFar2(averageCanValue_) || isNear2(averageCanValue_));

    // // POUR TESTER
    // uart_.uartTransmition(averageCanValue_);
    // if (isNear2(averageCanValue_))
    //     led_.changeColor(Color::RED);
    // else if (isFar2(averageCanValue_))
    //     led_.changeColor(Color::GREEN);
    //  else
    //     led_.changeColor(Color::NONE);

    return averageCanValue_;
}

void ObstacleDetector::setAverage()
{
    const uint8_t nValues = 10;
    int somme = 0;
    for (uint8_t i = 0; i < nValues; i++)
    {
        canValue_ = converter_.lecture(PA2);
        canValue_ = canValue_ >> 2;
        somme += canValue_;
    }
    averageCanValue_ = (somme / nValues);

    // Uart uart_ = Uart(Mode::RECEIVEANDTRANSMIT) ;
    // if (isObstacle()) {
    // uart_.uartTransmition(averageCanValue_);
    // uart_.uartTransmition(isObstacle());
    // }
    // _delay_ms(1000);
}

void ObstacleDetector::cleanReadings()
{
    for (uint8_t i = 0; i < maxReadings_; i++)
        readings_[i] = 0;

    readingCounter_ = 0;
}

bool ObstacleDetector::atLeastOneCloseDetection()
{
    for (uint8_t i = 0; i < maxReadings_; i++)
        if (isNear2(readings_[i]))
            return true;

    return false;
}

void ObstacleDetector::setLastDetection(DetectionState state)
{
    lastDetection_ = state;
}

bool ObstacleDetector::checkForObstacle2(void)
{
    detect();
    if (isNear2(averageCanValue_))
        lastDetection_ = DetectionState::CLOSE;
    else
        lastDetection_ = DetectionState::FAR;
    startObstacleDetectedEvent();

    // detect();
    // bool obstacleDetected = false;
    // uint8_t readingCounter = 0;
    // led_.changeColor(Color::NONE);
    // while (isObstacle_) {
    //     obstacleDetected = true;
    //     readings_[readingCounter] = averageCanValue_;
    //     detect();
    //     readingCounter++;
    //     // led_.changeColor(Color::GREEN);
    // }
    // led_.changeColor(Color::NONE);

    // if (!obstacleDetected)
    //     return false;
    // else {
    // for (uint8_t i = 0; i < maxReadings_; i++)
    //     uart_.uartTransmition(readings_[i]);

    //     if (atLeastOneCloseDetection())
    //         lastDetection_ = DetectionState::CLOSE;
    //     else
    //         lastDetection_ = DetectionState::FAR;

    //     startObstacleDetectedEvent();
    //     cleanReadings();
    //     // determineObstacleToStock(); // TODO: verifier cette fonction
    //     return true;
    // }
}

// bool ObstacleDetector::checkForObstacle(void)
// {
//     // ALGORITHM POUR CALCULER LE SEUL POTEAU PROCHE
//     setAverage();
//     bool obstacleDetected = false;
//     uint8_t readingCounter = 0;
//     while (isObstacle()) {
//         obstacleDetected = true;
//         readings_[readingCounter] = averageCanValue_;
//         setAverage();
//         readingCounter++;
//     }
//     if (!obstacleDetected)
//         return false;
//     else {
//         bool atLeastOneNearDetection = false;
//         for (uint8_t i = 0; i < maxReadings_; i++) {
//             averageCanValue_ = readings_[i];
//             if (isNear())
//                 atLeastOneNearDetection = true;
//         }
//         if (atLeastOneNearDetection) {
//             piezo_.startSound(75);
//             led_.changeColor(Color::RED);
//             _delay_ms(1000);
//             piezo_.stopSound();
//             led_.changeColor(Color::NONE);
//         }
//         else
//         {
//             piezo_.startSound(50);
//             led_.changeColor(Color::GREEN);
//             _delay_ms(1000);
//             piezo_.stopSound();
//             led_.changeColor(Color::NONE);
//         }
//         for (uint8_t i = 0; i < maxReadings_; i++)
//             readings_[i] = 0;
//         return true;
//     }

// DERNIER CODE FONCTIONNEL
// setAverage();
// if (isObstacle())
// {
//     // When we have a far detection, we have to make sure that is far
//     // if (isFar()) {
//     //     _delay_ms(15);
//     //     setAverage();
//     // }
//     // determineObstacleToStock();
//     if (isNear())
//         lastDetection_ = DetectionState::CLOSE;
//     else
//         lastDetection_ = DetectionState::FAR;
//     startObstacleDetectedEvent();
//     return true;
//     // if (isNear) {
//     //     // When we have a near detection, we can be sure that is near
//     //     determineObstacleToStock();
//     //     return true; // TODO: peut-etre changer ce return de function par un attribut
//     // } else {
//     //     setAverage();
//     // }
//     // setAverage(); // to confirm obstacle
//     // if (isObstacle()) {
//     //     determineObstacleToStock();
//     //     return true;
//     // }
// }
// led_.changeColor(Color::NONE);
// return false;
// }

void ObstacleDetector::startObstacleDetectedEvent(void)
{
    if (lastDetection_ == DetectionState::CLOSE)
    {
        piezo_.startSound(75);
        led_.changeColor(Color::RED);
        _delay_ms(1000);
        piezo_.stopSound();
        led_.changeColor(Color::NONE);
    }
    else
    {
        piezo_.startSound(50);
        led_.changeColor(Color::GREEN);
        _delay_ms(1000);
        piezo_.stopSound();
        led_.changeColor(Color::NONE);
    }
}

// TODO: Cette methode est tres repetitive mais jsuis pas sur si la separer ou pas en une autre methode
void ObstacleDetector::determineObstacleToStock(void)
{
    if (!isDeterminingObstaclesFinished && isObstacle())
    {
        if (numObstacles_ == 0)
        {
            if (isNear2(averageCanValue_))
            {
                locationOfObstacles_[0] = static_cast<uint8_t>(Point::N);
            }
            else
            {
                locationOfObstacles_[0] = static_cast<uint8_t>(Point::O);
                
            }
        }
        else if (numObstacles_ == 1)
        {
            if (isNear2(averageCanValue_))
            {
                locationOfObstacles_[1] = static_cast<uint8_t>(Point::HF);
            }
            else
            {
                locationOfObstacles_[1] = static_cast<uint8_t>(Point::IG);
                
            }
        }
        else
        {
            if (isNear2(averageCanValue_))
            {
                locationOfObstacles_[2] = static_cast<uint8_t>(Point::K);
            }
            else
            {
                locationOfObstacles_[2] = static_cast<uint8_t>(Point::L);
                
            }
            verifySwapForSpecialCase();
        }

        if (numObstacles_ != obstaclesSize_)
        {
            numObstacles_++;
        }
        else
        {
            isDeterminingObstaclesFinished = true;
        }
    }
}

// Switches order if three obstacles are detected
void ObstacleDetector::verifySwapForSpecialCase(void)
{
    if (locationOfObstacles_[1] == static_cast<uint8_t>(Point::IG) && locationOfObstacles_[2] == static_cast<uint8_t>(Point::K))
    {
        locationOfObstacles_[1] = static_cast<uint8_t>(Point::L);
        locationOfObstacles_[2] = static_cast<uint8_t>(Point::HF);
    }

    else if (locationOfObstacles_[1] == static_cast<uint8_t>(Point::HF) && locationOfObstacles_[2] == static_cast<uint8_t>(Point::L))
    {
        locationOfObstacles_[1] = static_cast<uint8_t>(Point::K);
        locationOfObstacles_[2] = static_cast<uint8_t>(Point::IG);
    }

    else
    {
        uint8_t temp = locationOfObstacles_[1];
        locationOfObstacles_[1] = locationOfObstacles_[2];
        locationOfObstacles_[2] = temp;
    }
}

// Returns list of obstacles stocked in memory in reverse order.
uint8_t *ObstacleDetector::retrieveObstacleList(uint8_t *list)
{
    uint8_t dataRead;
    for (uint16_t i = 0x0002; i > sizeof(numObstacles_); i--)
    {
        memory_.lecture(i, &dataRead);
        list[i] = dataRead;
    }
    return list;
}

void ObstacleDetector::stockObstacle(int i)
{
    memory_.ecriture(i, locationOfObstacles_[i]);
    uart_.uartTransmition(locationOfObstacles_[i]);
    _delay_ms(10);
}

// Must be called when robot stops at B.
void ObstacleDetector::stockObstacleList(void)
{
    // WIPE MEMORY
    for (uint16_t i = 0x0000; i < 0x0003; i++)
	{
		memory_.ecriture(i, 0);
		_delay_ms(10);
	}

    for (uint16_t i = 0x00; i < numObstacles_; i++)
    {
        memory_.ecriture(i, locationOfObstacles_[i]);
        _delay_ms(10);
    }
}

bool ObstacleDetector::isThereThreeObstacles(void)
{
    if (numObstacles_ == 3)
    {
        return true;
    }
    return false;
}