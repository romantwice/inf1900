//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Definition de la classe Piezo.

/*
	Use timer2 (Page 138) in CTC mode to generate sound wave send to the piezo speaker.
	frequency = f_clk_io/(2*N*(1 + OCRnx)) - N = prescaler value
	N = 256 - frequency = [61,15625]
	Frequency = 1200 - N = 256, OCR2A = 12
	PD7 = signalPin = OC2A
	PD6 = need to be clear on program for ground reference
*/

#pragma once
#ifndef PIEZO_H
#define PIEZO_H

#include <avr/io.h>
#include <util/delay.h>

class Piezo
{
public:
	Piezo();
	~Piezo();

	/*
	Timer2 (Page 138) in CTC mode (Page 143), OC2A on toggle mode, OC2B not use.
	Waveform generation mode (page 153) Mode 2
	Prescaler of 256
	*/
	void initTimer0() const;
	void startSound(uint8_t note) const;
	void stopSound() const;

private:
	int signalPin_ = PD7;
};

#endif // !PIEZO_H