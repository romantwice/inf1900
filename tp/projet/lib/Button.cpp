//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Implementation de la classe button.

#ifndef F_CPU
#define F_CPU 8000000UL
#endif

#include "Button.h"

Button::Button(ButtonType type) : type_(type) {
    mask_ = (type_ == ButtonType::EXTERNAL) ? externMask_ : internMask_;
}

bool Button::isPressing() const
{
    bool didAlreadyPress = false;
    if (PIND & mask_)
        didAlreadyPress = true;

    _delay_ms(debouncingDelay_);
    bool debouncedResult = (PIND & mask_) && didAlreadyPress;

    // Invert logic if external button 
    return (type_ == ButtonType::EXTERNAL) ? !debouncedResult : debouncedResult;
}

bool Button::didRelease() const
{
    bool didAlreadyPress = isPressing();
    
    // this while make sure that we wait for the user to release the button
    while (didAlreadyPress && isPressing()) {} 
    
    return didAlreadyPress;
}