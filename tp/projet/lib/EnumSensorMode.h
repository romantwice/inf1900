//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      

#ifndef SENSORMODE_H
#define SENSORMODE_H

enum class SensorMode
{
    FOLLOWLINE,
    HALLWAY,
    OFF
};

#endif //!SENSORMODE_H