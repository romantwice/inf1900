//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      J�r�mie Leclerc #1854461
//
//  Description:
//      Implementation de la classe wheel.

#ifndef F_CPU
#define F_CPU 8000000UL
#endif

#include "Wheel.h"
#include <avr/io.h>

Wheel::Wheel(const int leftWheelPinDirection, const int rightWheelPinDirection)
{
	initRegister();
	leftWheelPinDirection_ = leftWheelPinDirection;
	rightWheelPinDirection_ = rightWheelPinDirection;
}

Wheel::~Wheel()
{
}

void Wheel::initRegister()
{
	TCCR0A |= (1 << WGM00) | (1 << COM0A1) | (1 << COM0B1);
	TCCR0A &= ~((1 << WGM01) | (1 << COM0A0) | (1 << COM0B0));
	TCCR0B |= (1 << CS01);
	TCCR0B &= ~((1 << CS00) | (1 << CS02) | (1 << WGM02));
}

void Wheel::ajustSpeed(uint8_t percentLeftWheel, uint8_t percentRightWheel)
{
	OCR0A = percentLeftWheel; 
	OCR0B = percentRightWheel;
}

void Wheel::leftWheelRunForward()
{
	PORTB &= ~(1 << leftWheelPinDirection_);
}

void Wheel::leftWheelRunBackward()
{
	PORTB |= (1 << leftWheelPinDirection_);
}

void Wheel::rightWheelRunForward()
{
	PORTB &= ~(1 << rightWheelPinDirection_);
}

void Wheel::rightWheelRunBackward()
{
	PORTB |= (1 << rightWheelPinDirection_);
}

void Wheel::runStraightForward(const uint8_t speedLeftWheel, const uint8_t speedRightWheel)
{
	rightWheelRunForward();
	leftWheelRunForward();
	ajustSpeed(speedLeftWheel, speedRightWheel);
}

void Wheel::runStraightBackward(const uint8_t speedLeftWheel, const uint8_t speedRightWheel)
{
	rightWheelRunBackward();
	leftWheelRunBackward();
	ajustSpeed(speedLeftWheel, speedRightWheel);
}

void Wheel::stopWheel()
{
	ajustSpeed(0x00, 0x00);
}
