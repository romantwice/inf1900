//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Definition de la classe Reader.

#ifndef READER_H
#define READER_H

#include <memory_24.h>
#include <Uart.h>
#include <util/delay.h>

class Reader
{
public:
    Reader();
    ~Reader();
    void readData(uint8_t position, uint8_t *dataRead);
    uint16_t getByteCodeSize();
    void writeByteCodeInMemory();
    void writeByteCodeSize();
    void readAllMemory();

private:
    // Comment faire pour que ceci ne cree pas une nouvelle instance de ces classes:
    // 1- passer instance au constrcuteur
    // 2- singleton ces classes la
    Memoire24CXXX memory_ = Memoire24CXXX();
    Uart uart_ = Uart(Mode::RECEIVEANDTRANSMIT);

};
#endif //! READER_H