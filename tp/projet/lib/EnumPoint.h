//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Definition des points du circuit

#ifndef ENUMPOINT_H
#define ENUMPOINT_H
//Idee: constructeur
//Pas possible

// Points ou osbtacles pourraient etre places : FGHI et ON, plus optionnel KL

enum class Point
{
    A,
    B,
    C,
    D,
    E,

    HF = 72,
    IG = 73,
    J,
    K = 75,
    L = 76,
    M,
    N = 78,
    O = 79,
    P,
    Q,
    R,
    S,
    T,
    T1,
    U,
    V,
    W,
    X,
    Y,
    YELLOWPOINTUPLEFT,
    YELLOWPOINTUPRIGHT,
    YELLOWPOINTUDOWNLEFT,
    YELLOWPOINTDOWNRIGHT,
    END
};

#endif // !ENUMPOINT_H
