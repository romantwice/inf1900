//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Implementation de la classe Piezo.

#include "Piezo.h"

Piezo::Piezo()
{
	initTimer0();
}

Piezo::~Piezo()
{
}

void Piezo::initTimer0() const
{
	TCCR2A |= (1 << COM2A0) | (1 << WGM21);
	TCCR2A &= ~((1 << COM2A1) | (1 << WGM20));
	TCCR2B &= ~(1 << CS20) | (1 << WGM20);
}

void Piezo::startSound(uint8_t note) const // use 0x0C for 1200hz bip
{
	TCCR2B |= (1 << CS21) | (1 << CS22);
	switch (note)
	{
	case 45:
		OCR2A = 0x8D;
		break;
	case 46:
		OCR2A = 0x85;
		break;
	case 47:
		OCR2A = 0x7E;
		break;
	case 48:
		OCR2A = 0x76;
		break;
	case 49:
		OCR2A = 0x6F;
		break;
	case 50:
		OCR2A = 0x69;
		break;
	case 51:
		OCR2A = 0x63;
		break;
	case 52:
		OCR2A = 0x5E;
		break;
	case 53:
		OCR2A = 0x58;
		break;
	case 54:
		OCR2A = 0x53;
		break;
	case 55:
		OCR2A = 0x4F;
		break;
	case 56:
		OCR2A = 0x4A;
		break;
	case 57:
		OCR2A = 0x46;
		break;
	case 58:
		OCR2A = 0x42;
		break;
	case 59:
		OCR2A = 0x3E;
		break;
	case 60:
		OCR2A = 0x3B;
		break;
	case 61:
		OCR2A = 0x37;
		break;
	case 62:
		OCR2A = 0x34;
		break;
	case 63:
		OCR2A = 0x31;
		break;
	case 64:
		OCR2A = 0x2E;
		break;
	case 65:
		OCR2A = 0x2C;
		break;
	case 66:
		OCR2A = 0x29;
		break;
	case 67:
		OCR2A = 0x27;
		break;
	case 68:
		OCR2A = 0x25;
		break;
	case 69:
		OCR2A = 0x23;
		break;
	case 70:
		OCR2A = 0x21;
		break;
	case 71:
		OCR2A = 0x1F;
		break;
	case 72:
		OCR2A = 0x1D;
		break;
	case 73:
		OCR2A = 0x1B;
		break;
	case 74:
		OCR2A = 0x1A;
		break;
	case 75:
		OCR2A = 0x18;
		break;
	case 76:
		OCR2A = 0x17;
		break;
	case 77:
		OCR2A = 0x15;
		break;
	case 78:
		OCR2A = 0x14;
		break;
	case 79:
		OCR2A = 0x13;
		break;
	case 80:
		OCR2A = 0x12;
		break;
	case 81:
		OCR2A = 0x11;
		break;
	default:
		TCCR2B &= ~((1 << CS21) | (1 << CS22));
		break;
	}
}
void Piezo::stopSound() const
{
	TCCR2B &= ~((1 << CS21) | (1 << CS22));
}
