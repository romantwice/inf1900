//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Definition des angles de rotation du robot (en degres)

#ifndef ENUMANGLE_H
#define ENUMANGLE_H

enum class Angle
{
    NINETY = 87,  // x * 25 ms delay for robot to turn - to test
    FIFTEEN = 20, // x * 25 ms delay for robot to turn - to test 
    TIRTY = 40,
    ZERO
};

#endif //!ENUMANGLE_H