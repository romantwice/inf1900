#pragma once
//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      J�r�mie Leclerc #1854461
//
//  Description:
//      Implementation de la classe wheel.

/*
Timer0 in PWM phase correct, fix TOP value to 0xff. Set OC0X on Compare Match when up-counting.
Clear OC0X on Compare Match when down-counting .
OC0A = PB3 - Left wheel
OC0B = PB4 - Right wheel
PB2  = direction signal left wheel
PB5  = direction signal right wheel
 */

#ifndef WHEEL_H
#define WHEEL_H

#include <avr/io.h>

class Wheel
{
public:
	/*  Wheel::Wheel(int leftWheelPinDirection, int rightWheelPinDirection)
		 Constructor : Proceed to initialization
	In parameter  : int leftWheelPinDirection, int rightWheelPinDirection - Output signal pin to send direction signal to motor
	Out parameter : Wheel object */
	Wheel(const int leftWheelPinDirection, const int rightWheelPinDirection);

	/*  Wheel::~Wheel()
		Destructor : Do nothing */
	~Wheel();

	/*  void Wheel::initRegister()
		 Initialize the register of the timer0 in PWM phase correct, TOP 0xFF mode. Called
		 by the constructor.
	TCCR0A : COM0A0, COM0A1, COM0B0, COM0B1 - Set OC0B on Compare Match when up-counting. Clear OC0B on
	Compare Match when down-counting (Page 102)
				WGM00, WGM01 - Mode 1 (Page 103)
	TCCR0B : CS00, CS01, CS02 - Prescaler at clk/8 (Page 104)
				WGM02 - Mode 1 (Page 103) */
	void initRegister();

	/*  void Wheel::ajustSpeed(uint8_t percentLeftWheel, uint8_t percentRightWheel)

	In parameter  : uint8_t percentLeftWheel  - Value between 0 and 255 - 255 is highest speed.
												Value down to 0x70 is not enought to move the wheel
	Out parameter : uint8_t percentRightWheel - Value between 0 and 255 - 255 is highest speed.
												Value down to 0x70 is not enought to move the wheel*/
	void ajustSpeed(const uint8_t percentLeftWheel, const uint8_t percentRightWheel);

	/* Set the direction port for each wheel to backward or forward mode
   In parameter  : volatile uint8_t& port
   Out parameter : none */
	void leftWheelRunForward();
	void leftWheelRunBackward();
	void rightWheelRunForward();
	void rightWheelRunBackward();

	void runStraightForward(const uint8_t speedLeftWheel, const uint8_t speedRightWheel);
	void runStraightBackward(const uint8_t speedLeftWheel, const uint8_t speedRightWheel);
	void turn90degreeRight();
	void turn90degreeLeft();
	void stopWheel();

private:
	int leftWheelPinDirection_;
	int rightWheelPinDirection_;
};

#endif // !WHEEL_H
