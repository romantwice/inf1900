//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Definition de la classe button.

#pragma once
#ifndef BUTTON_H
#define BUTTON_H

#include <avr/io.h>
#include <util/delay.h>

enum class ButtonType
{
    INTERNAL,
    EXTERNAL
};

class Button
{
public:

    Button(ButtonType type = ButtonType::INTERNAL);

    // Check if we are pressing a button by the polling method.
    // We have 2 buttons:
    //  - The internal (PD2)
    //  - The external (PD3)
    // So the DDRD associated pins must be setted to the input mode.
    // This function does the needed logic inversion when 
    // we use the external button.
    bool isPressing() const;

    bool didRelease() const;

private:
    ButtonType type_; 
    const static uint8_t internMask_ = 0b00000100;
    const static uint8_t externMask_ = 0b00001000;
    uint8_t mask_;
    const static uint8_t debouncingDelay_ = 10; // ms
};
#endif // !BUTTON_H