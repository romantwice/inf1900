//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      J�r�mie Leclerc #1854461
//
//  Description:
//      

#ifndef ENUMINSTRUCTIONMSK_H
#define ENUMINSTRUCTIONMSK_H

enum class InstructionMsk
{
	ONLINE 		  = 0b00010000,
	ISLITTLELEFT  = 0b00110000,
	IS90LEFT	  = 0b01110000,
	ISLITTLERIGHT = 0b00011000,
	IS90RIGHT	  = 0b00011100,
	ISFARLEFT	  = 0b01100000,
	ISFARRIGHT	  = 0b00001100,
	ISFULLLINE	  = 0b01111100, 
	ISDOUBLELINE  = 0b01101100,
	ISNOTHING	  = 0b00000000,
	ISSTOP		  
};

#endif //!ENUMINSTRUCTIONMSK_H
