//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Definition des directions du robot

#ifndef ENUMDIRECTION_H
#define ENUMDIRECTION_H

enum class Direction
{
    FORWARD,
    BACKWARD,
    LEFT,
    RIGHT
};

#endif //!ENUMDIRECTION_H