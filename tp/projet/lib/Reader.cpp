//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Implementation de la classe Reader.

#include <Reader.h>
Reader::Reader()
{
}

Reader::~Reader()
{
}

void Reader::readData(uint8_t position, uint8_t *dataRead)
{
    memory_.lecture(position, dataRead);
}

void Reader::writeByteCodeSize()
{
    uint8_t dataRead1;
    uint8_t dataRead2;
    dataRead1 = uart_.uartReceive();
    memory_.ecriture(0, dataRead1);
    dataRead2 = uart_.uartReceive();
    memory_.ecriture(1, dataRead2);
}

// Read the first two bytes to get size of the file, a 16 bits value
uint16_t Reader::getByteCodeSize() {
    uint8_t dataRead1;
    uint8_t dataRead2;
    memory_.lecture(0, &dataRead1);
    uart_.uartTransmition(dataRead1);
    memory_.lecture(1, &dataRead2);
    uart_.uartTransmition(dataRead2);
    uint16_t result = dataRead1; 
    result = result << 8;
    result += dataRead2;
    return result;
}

void Reader::writeByteCodeInMemory()
{
    const int WRITING_DELAY = 5000; // trop, a changer

    writeByteCodeSize();
    const uint16_t size = getByteCodeSize();
    uint8_t test;

    for (uint16_t i = 2; i < (size + 2); i++)
    {
        test = uart_.uartReceive();
        memory_.ecriture(i, test);
    }
    _delay_ms(WRITING_DELAY);
}

// To only use to read from memory in RS-232 transmission to the pc.
void Reader::readAllMemory()
{
    for (uint16_t i = 0x0000; i < 0xFFFF; i++)
    {
        uint8_t dataRead;
        memory_.lecture(i, &dataRead);
        uart_.uartTransmition(dataRead);
    }
}
