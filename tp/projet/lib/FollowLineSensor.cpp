#include "FollowLineSensor.h"

FollowLineSensor::FollowLineSensor()
{
	// dataRead = 0x00;
	PORTC |= (1 << PC7); // Fix calibration signal to high for doing nothing
}

FollowLineSensor::~FollowLineSensor()
{
}

void FollowLineSensor::readSensors()
{
	uint8_t dataRead = PINC;
	if (dataRead & (1 << PC2)) { O1_ = true; } else { O1_ = false; }
	if (dataRead & (1 << PC3)) { O2_ = true; } else { O2_ = false; }
	if (dataRead & (1 << PC4)) { O3_ = true; } else { O3_ = false; }
	if (dataRead & (1 << PC5)) { O4_ = true; } else { O4_ = false; }
	if (dataRead & (1 << PC6)) { O5_ = true; } else { O5_ = false; }
}

void FollowLineSensor::saveLastRead()
{
	saveRead_[0] = O1_;
	saveRead_[1] = O2_;
	saveRead_[2] = O3_;
	saveRead_[3] = O4_;
	saveRead_[4] = O5_;
}

void FollowLineSensor::getLastRead()
{
	O1_ = saveRead_[0];
	O2_ = saveRead_[1];
	O3_ = saveRead_[2];
	O4_ = saveRead_[3];
	O5_ = saveRead_[4];
}

bool FollowLineSensor::isPoint() const
{
	return false;
}

InstructionMsk FollowLineSensor::getInstruction() 
{
	readSensors();
	if (!(O1_ | O2_ | O3_ | O4_ | O5_)) { getLastRead();}
	if (O1_ && O2_ && O3_ && O4_ && O5_) { return InstructionMsk::ISFULLLINE; }
	if ((O3_ && !(O2_ ^ O4_)) && !(O1_ | O5_)) { return InstructionMsk::ONLINE; }
	if ((O3_ && O4_ && O5_) | (O1_ && O2_ && O3_)) {} // Eliminate the case for the inner testing. Need testing
	 else {
		if ((O2_) && !(O5_ | O4_ )) { return InstructionMsk::ISLITTLERIGHT; }
		if ((O4_) && !(O1_ | O2_ )) { return InstructionMsk::ISLITTLELEFT; } }
	// if ((O1_ && O2_) && !(O3_ | O4_ | O5_)) { return InstructionMsk::ISLITTLELEFT; }
	// if ((O4_ && O5_) && !(O1_ | O2_ | O3_)) { return InstructionMsk::ISLITTLERIGHT; }
	if (((O1_ || O2_ ) && (O4_ || O5_)) && !(O3_)) { return InstructionMsk::ISDOUBLELINE; }
	saveLastRead();
	if (O1_) { return InstructionMsk::ISFARRIGHT; }
	if (O5_)  { return InstructionMsk::ISFARLEFT; }
	return InstructionMsk::ISNOTHING;
}

InstructionMsk FollowLineSensor::getHallwayInstruction()
{
	readSensors();
	if ((O1_ || O2_) && !(O3_ | O4_ | O5_)) { return InstructionMsk::ISFARLEFT; }
	if ((O4_ || O5_) && !(O1_ | O2_ | O3_)) { return InstructionMsk::ISFARRIGHT; }
	return InstructionMsk::ISNOTHING;
}

void FollowLineSensor::setLastRead(bool dataToSave[5])
{
	for (int i = 0; i < 4; i++)
	{
		saveRead_[i] = dataToSave[i];
	}
}
