//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Definition du controle de la DEL libre.

#ifndef LEDCONTROL_H
#define LEDCONTROL_H

#ifndef F_CPU
#define F_CPU 8000000UL
#endif

#include <avr/io.h>
#include <util/delay.h>
#include <EnumColor.h>

class LEDControl
{
public:
    LEDControl();
    ~LEDControl();
    void changeColor(Color color) const;
    void blink(Color color, int nBlink) const;
    bool checkForColorAmber(Color color) const;

private:
    Color color_ = Color::NONE;
    int pinIn_;
    int pinOut_;
    volatile uint8_t &port_ = PORTA; //TODO a entrer explicitement sans attribut
};

#endif // !LEDCONTROL_H