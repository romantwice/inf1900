//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//      Lylia Bouricha #1945124
//      Jérémie Leclerc #1854461
//
//  Description:
//      Implementation du controle de la DEL libre.

#include <LEDControl.h>

LEDControl::LEDControl()
{
    pinIn_ = PA0;
    pinOut_ = PA1;
}

LEDControl::~LEDControl()
{
}

void LEDControl::changeColor(Color color) const
{

    switch (color)
    {
    case Color::RED:
        port_ &= ~(1 << pinIn_);
        port_ |= 1 << pinOut_;
        break;
    case Color::AMBER:
    {
        checkForColorAmber(color);
        break;
    }

    case Color::GREEN:
        port_ &= ~(1 << pinOut_);
        port_ |= 1 << pinIn_;
        break;

    case Color::NONE:
        port_ &= (0 << pinIn_) & (0 << pinOut_);
        break;

    default:
        break;
    }
}

void LEDControl::blink(Color color, int nBlink) const
{
    for (int i = 0; i < nBlink; i++)
    {
        changeColor(color);
        _delay_ms(125); // TODO: remplacer chiffre magique
        changeColor(Color::NONE);
        _delay_ms(125); // TODO: remplacer chiffre magique
    }
}

// Peut etre bloquante????
bool LEDControl::checkForColorAmber(Color color) const
{
    // while (color == Color::AMBER)
    // {
    //     changeColor(Color::RED);
    //     _delay_ms(15);
    //     changeColor(Color::GREEN);
    //     _delay_ms(15);
    // }
}
