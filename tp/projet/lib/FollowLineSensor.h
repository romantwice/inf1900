#pragma once
#include <avr/io.h>
#include "EnumInstructionMsk.h"


#ifndef FOLLOWLINESENSOR_H
#define FOLLOWLINESENSOR_H

class FollowLineSensor
{
public:
	FollowLineSensor();
	~FollowLineSensor();

	/*  void FollowLineSensor::readSensors()
			Read the input of the LSS05 sensor - PINC2 to PINC6
	*/
	void readSensors();
	/*	void FollowLineSensor::saveLastRead()
			Save the last read in case the LSS05 sensor got off line
	*/
	void saveLastRead();
	/*	void FollowLineSensor::getLastRead()
			Replace the data with the last read because the LSS05 is off line.
			Last read is the outter sensor so it give indication of the direction
			to retrieve the line.
	*/
	void getLastRead();

	/*	void FollowLineSensor::getInstruction()
			
	*/
	InstructionMsk getInstruction();
	InstructionMsk getHallwayInstruction();
	void setLastRead(bool dataToSave[5]);
	bool isPoint() const;

private:
	bool O1_ = false;
	bool O2_ = false;
	bool O3_ = false;
	bool O4_ = false;
	bool O5_ = false;
	bool saveRead_[5];
};

#endif //FOLLOWLINESENSOR_H
