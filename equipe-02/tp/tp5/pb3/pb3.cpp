//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//
//  Description:
//
//  Identifications matérielles (Broches I/O):
//                      Tableau d'états

#include <util/delay.h>
#include <avr/eeprom.h>
#include <memoire_24.h>
using namespace std;

const uint8_t WRITING_DELAY = 5000;
const uint8_t ADDRESS = 0x0000;
const uint8_t LED_DELAY = 1000;

enum class Color
{
    RED,
    AMBER,
    GREEN,
    NONE
};

void changeColor(Color color)
{
    switch (color)
    {
    case Color::RED:
        PORTA &= ~(1 << PA0);
        PORTA |= 1 << PA1;
        break;

    case Color::AMBER:
    {
        changeColor(Color::RED);
        _delay_ms(LED_DELAY);
        changeColor(Color::GREEN);
        _delay_ms(LED_DELAY);
        break;
    }

    case Color::GREEN:
        PORTA &= ~(1 << PA1);
        PORTA |= 1 << PA0;
        break;

    case Color::NONE:
        PORTA &= (0 << PA0) & (0 << PA1);
        break;

    default:
        break;
    }
}

void initialiseRegisters()
{
    DDRA |= (1 << PA0) | (1 << PA1);
}

void initialisationUART(void)
{
    // 2400 bauds. Nous vous donnons la valeur des deux
    // premiers registres pour vous éviter des complications.
    UBRR0H = 0;
    UBRR0L = 0xCF;
    // permettre la réception et la transmission par le UART0
    UCSR0B |= (1 << RXEN0) | (1 << TXEN0);
    // Format des trames: 8 bits, 1 stop bits, sans parité
    UCSR0C &= ~(1 << USBS0);
    UCSR0B &= ~(1 << UCSZ02);                // Trame de 8 bits
    UCSR0C |= (1 << UCSZ01) | (1 << UCSZ00); // Trame de 8 bits
}

// Pourquoi on utilise pas le memes registres que la fonction
// init UART?
// UCSR0A = (1 << MPCM0); -> On n'a pas besoin pour cet exo.

// PAS BESOIN: mais pour comprehension et savoir existence
unsigned char USART_Receive(void)
{
    /* Wait for data to be received */
    while (!(UCSR0A & (1 << RXC0)))
        ;
    /* Get and return received data from buffer */
    return UDR0;
}

// Du USART vers le PC
void transmissionUART(uint8_t data)
{
    /* Wait for empty transmit buffer */
    while (!(UCSR0A & (1 << UDRE0)))
        ;
    /* Put data into buffer, sends the data */
    UDR0 = data;
}

int main()
{
    initialiseRegisters();
    initialisationUART();
    
    Memoire24CXXX memory = Memoire24CXXX();
    uint8_t message[22] = {"Le robot en INF1900\n"};

    changeColor(Color::GREEN);
    memory.ecriture(ADDRESS, message, sizeof(message));
    _delay_ms(WRITING_DELAY);

    for (uint8_t i = 0x00; i < 0xFF; i++)
    {
        int j = 0;
        uint8_t messageRead[j];
        memory.lecture(ADDRESS, messageRead, sizeof(messageRead));
        transmissionUART(messageRead[j]); // De la memoire eeprom externe 
        if(messageRead[i] == 0xFF) break;
        j++;
    }

    changeColor(Color::RED);
    _delay_ms(LED_DELAY);
}