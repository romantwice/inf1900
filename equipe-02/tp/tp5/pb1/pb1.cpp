//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//
//  Description:
//
//  Identifications matérielles (Broches I/O):
//                      Tableau d'états

#define F_CPU 8000000UL
#include <util/delay.h>
#include <memoire_24.h>

using namespace std;

const uint8_t WRITING_DELAY = 5000;
const uint8_t LED_DELAY = 1000;
const uint8_t ADDRESS = 0x0000;
const uint8_t SIZE = 45;

enum class Color
{
    RED,
    AMBER,
    GREEN,
    NONE
};

void initialiseRegisters()
{
    DDRA |= (1 << PA0) | (1 << PA1);
}

void changeColor(Color color)
{
    switch (color)
    {
    case Color::RED:
        PORTA &= ~(1 << PA0);
        PORTA |= 1 << PA1;
        break;

    case Color::AMBER:
    {
        changeColor(Color::RED);
        _delay_ms(LED_DELAY);
        changeColor(Color::GREEN);
        _delay_ms(LED_DELAY);
        break;
    }

    case Color::GREEN:
        PORTA &= ~(1 << PA1);
        PORTA |= 1 << PA0;
        break;

    case Color::NONE:
        PORTA &= (0 << PA0) & (0 << PA1);
        break;

    default:
        break;
    }
}

int main()
{
    initialiseRegisters();
    Memoire24CXXX memory = Memoire24CXXX();

    uint8_t messageWrote[] = {"*P*O*L*Y*T*E*C*H*N*I*Q*U*E**M*O*N*T*R*E*A*L*"};
    memory.ecriture(ADDRESS, messageWrote, sizeof(messageWrote));
    _delay_ms(WRITING_DELAY);

    // Ajouter aussi 0x00 en mem. externe
    uint8_t messageRead[sizeof(messageWrote)];
    memory.lecture(ADDRESS, messageRead, sizeof(messageRead));

    changeColor(Color::RED);
    _delay_ms(LED_DELAY);

    for (int i = 0; i < sizeof(messageRead); i++)
    {
        while (messageWrote[i] == messageRead[i])
        {
            changeColor(Color::GREEN);
            _delay_ms(LED_DELAY);
        }
    }
}
