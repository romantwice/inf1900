//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//
//  Description: 
//
//  Identifications matérielles (Broches I/O):
//                      Tableau d'états

#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
#include "memoire_24.h"
using namespace std;

void initialisationUART ( void ) {
    // 2400 bauds. Nous vous donnons la valeur des deux
    // premiers registres pour vous éviter des complications.
    UBRR0H = 0;
    UBRR0L = 0xCF;

    // permettre la réception et la transmission par le UART0
    // (USART Control and Status Register) 
    // UCSR0A = 'modifier ici' ; We don't need this register
    UCSR0B |= (1 << TXEN0) | (1 << RXEN0); 

    // Format des trames: 8 bits, 1 stop bits, sans parité
    UCSR0C &= ~(1 << USBS0); // 1 stop bit
    UCSR0B &= ~(1 << UCSZ02); // Character size in a frame (8 bits) 
    UCSR0C |= (1 << UCSZ01) | (1 << UCSZ00); // Character size in a frame (8 bits)
    UCSR0C &= ~(1 << UPM01) & ~(1 << UPM00); // no parity
}

void setupRegisters() {
    DDRA |= (1 << PA0) | (1 << PA1); 
    initialisationUART();
}

bool isUartBusy() {
    return !(UCSR0A & (1 << UDRE0));
}

// Du USART vers le PC
void transmissionUART ( uint8_t donnee ) {
    while (isUartBusy());
    UDR0 = donnee; 
}

int main()
{

    setupRegisters();
    Memoire24CXXX memory = Memoire24CXXX();

    uint8_t data[] = "hello\n";
    memory.ecriture(0, data, sizeof(data));

    _delay_ms(1000);

    // uint8_t dataRead[8];   
    // memory.lecture(0, dataRead, 8);

    // bool isSame = true;
        // if (dataRead[i] != data[i])
        //     isSame = false;

    // for (int times = 0; times < 20; times++)
    //     for (int i = 0; i < 6; i++)
    //         transmissionUART(dataRead[i]);

    for (int times = 0x00; times < 0xFF; times++) {
        uint8_t readData[1];
        memory.lecture(times, readData, 1);
        transmissionUART(readData[0]);
        // transmissionUART(" ");
    }
            
        // PORTA |= (1 << PA1);
        // PORTA &= ~(1 << PA0);
    

    // PORTA &= ~(1 << PA1);
    // PORTA &= ~(1 << PA0);
}