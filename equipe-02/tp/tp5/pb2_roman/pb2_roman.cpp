//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//
//  Description: 
//
//  Identifications matérielles (Broches I/O):
//                      Tableau d'états

#define F_CPU 8000000UL
#include <avr/eeprom.h>
#include <avr/io.h>
#include <util/delay.h>

void initialisationUART ( void ) {
    // 2400 bauds. Nous vous donnons la valeur des deux
    // premiers registres pour vous éviter des complications.
    UBRR0H = 0;
    UBRR0L = 0xCF;

    // permettre la réception et la transmission par le UART0
    // (USART Control and Status Register) 
    // UCSR0A = 'modifier ici' ; We don't need this register
    UCSR0B |= (1 << TXEN0) | (1 << RXEN0); 

    // Format des trames: 8 bits, 1 stop bits, sans parité
    UCSR0C &= ~(1 << USBS0); // 1 stop bit
    UCSR0B &= ~(1 << UCSZ02); // Character size in a frame (8 bits) 
    UCSR0C |= (1 << UCSZ01) | (1 << UCSZ00); // Character size in a frame (8 bits)
    UCSR0C &= ~(1 << UPM01) & ~(1 << UPM00); // no parity
}

bool isUartBusy() {
    return !(UCSR0A & (1 << UDRE0));
}

// Du USART vers le PC
void transmissionUART ( uint8_t donnee ) {
    while (isUartBusy());
    UDR0 = donnee; 
}

int main()
{
    while (true) {
        initialisationUART();
        uint8_t msg[14] = "hello world\n";
        for (int i = 0; i < 14; i++)
            transmissionUART(msg[i]);
    }
        DDRA |= (1 << PA0) | (1 << PA1);
        PORTA |= (1 << PA0);
}