//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//
//  Description:
//
//  Identifications matérielles (Broches I/O):
//                      Tableau d'états

#include <avr/eeprom.h>
#include <memoire_24.h>
using namespace std;

void initialisationUART(void)
{
    // 2400 bauds. Nous vous donnons la valeur des deux
    // premiers registres pour vous éviter des complications.
    UBRR0H = 0;
    UBRR0L = 0xCF;
    // permettre la réception et la transmission par le UART0
    UCSR0B |= (1 << RXEN0) | (1 << TXEN0);
    // Format des trames: 8 bits, 1 stop bits, sans parité
    UCSR0C &= ~(1 << USBS0);
    UCSR0B &= ~(1 << UCSZ02);                // Trame de 8 bits
    UCSR0C |= (1 << UCSZ01) | (1 << UCSZ00); // Trame de 8 bits
}

// Pourquoi on utilise pas le memes registres que la fonction
// init UART?
// UCSR0A = (1 << MPCM0); -> On n'a pas besoin pour cet exo.

// PAS BESOIN: mais pour comprehension et savoir existence
unsigned char USART_Receive(void)
{
    /* Wait for data to be received */
    while (!(UCSR0A & (1 << RXC0)))
        ;
    /* Get and return received data from buffer */
    return UDR0;
}

// Du USART vers le PC
void transmissionUART(uint8_t data)
{
    /* Wait for empty transmit buffer */
    while (!(UCSR0A & (1 << UDRE0)))
        ;
    /* Put data into buffer, sends the data */
    UDR0 = data;
}

void transmitMessage(void)
{
    char message[22] = "Le robot en INF1900\n";
    uint8_t i, j;
    for (i = 0; i < 100; i++)
    {
        for (j = 0; j < 22; j++)
        {
            transmissionUART(message[j]);
        }
    }
}

// Question intéressante (DEMANDER DORINE)
// Dans le cas du problème 2, combien de cycles d'horloge se produisent
// entre 2 caractères transmis. Serait-il mieux de transmettre
// par le port RS232 en contrôlant par un mode de scrutation
// ou d'interruption? Pourquoi?

// Bus va a combien de bit/s? Distance aussi? DOCUMENTATION

int main()
{
    initialisationUART();
    transmitMessage();
}