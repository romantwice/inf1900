//  Auteurs: 
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizalez #2089991
// 
//  Description: TP2. Machines à états finis logicielles.
// 
//  Identifications matérielles (Broches I/O):
//      D2 est connecté au button de la section interrupt, donc il faut mettre le cavalier sur IntEN.
//      A0 est connecté au positif de la Del libre.
//      A1 est connecté au négatif de la Del libre.
// 
//  Tableau d'états
// +--------------+----------+-----------------+--------------+--------+
// |    State     | Pressing | didAlreadyPress |  Next state  | Output |
// +--------------+----------+-----------------+--------------+--------+
// | START        | X        | false           | START        | NONE   |
// | START        | 1        | true            | START        | NONE   |
// | START        | 0        | true            | FIRST_PRESS  | NONE   |
// | FIRST_PRESS  | X        | false           | FIRST_PRESS  | NONE   |
// | FIRST_PRESS  | 1        | true            | FIRST_PRESS  | NONE   |
// | FIRST_PRESS  | 0        | true            | SECOND_PRESS | NONE   |
// | SECOND_PRESS | X        | false           | SECOND_PRESS | NONE   |
// | SECOND_PRESS | 1        | true            | SECOND_PRESS | NONE   |
// | SECOND_PRESS | 0        | true            | GREEN        | NONE   |
// | GREEN        | X        | X               | START        | GREEN  |
// +--------------+----------+-----------------+--------------+--------+
//
//  Pressing: Le fait de peser le bouton ou non, donc cela fait référence 
//  à la valeur de la broche 2 du port D (D2).
//
//  didAlreadyPress: Cette colonne n'est pas une entrée, mais plutôt une 
//  variable dans notre code (fonction didRelease()).

#define F_CPU 8000000UL

#include <avr/io.h>
#include <util/delay.h>
 
enum class LedState { START, FIRST_PRESS, SECOND_PRESS, GREEN };
enum class Color { NONE, GREEN, RED };

const uint8_t BUTTON_MASK = 0x04;
const int LED_ON_DELAY = 2000; // ms
const uint8_t DEBOUNCING_DELAY = 10; // ms
 
// a prendre 
void changeColor(Color color) {
    switch (color)
    {
    case Color::NONE :
        PORTA &= ~(1 << PA0) & ~(1 << PA1);
        break;

    case Color::GREEN :
        // Forcer XXXX XX0X
        PORTA |= 1 << PA0;
        PORTA &= ~(1 << PA1); 
        break;

    case Color::RED :
        // Forcer XXXX XXX0
        PORTA &= ~(1 << PA0);
        PORTA |= 1 << PA1; 
        break;
    
    default:
        break;
    }
}


// class button
bool didPress() {
    bool didPressOneTime = false;
    bool didPressButton = PIND & BUTTON_MASK;

    if (didPressButton) 
        didPressOneTime = true;

    _delay_ms(DEBOUNCING_DELAY);

    return didPressButton && didPressOneTime;
}

// class button
bool didRelease() {
    bool didAlreadyPress = false;
    while (didPress())
    {
        didAlreadyPress = true;
    } 
    
    return (didAlreadyPress) ? true : false;
}

int main() {
    DDRA |= (1 << PA0) | (1 << PA1); // Forcer XXXX XX11 (A0 et A1 en sortie)
    DDRD &= ~(1 << PD2); // Forcer XXXX X0XX (D2 en entrée)

    LedState ledState { LedState::START };

    while (true)
    {
        switch (ledState)
        {
        case LedState::START :
            changeColor(Color::NONE);
            if (didRelease()) 
                ledState = LedState::FIRST_PRESS;
            break;

        case LedState::FIRST_PRESS : 
            if (didRelease()) 
                ledState = LedState::SECOND_PRESS;
            break;

        case LedState::SECOND_PRESS : 
            if (didRelease()) 
                ledState = LedState::GREEN;
            break;

        case LedState::GREEN :
            changeColor(Color::GREEN);
            _delay_ms(LED_ON_DELAY);
            ledState = LedState::START;
            break;

        default:
            break;
        }
    }
}