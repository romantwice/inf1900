//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//
//  Description:
//
//  Identifications matérielles (Broches I/O):
//

#include <iostream>
#include <avr/interrupt.h>

bool didPress(uint8_t MASK)
{
    bool didPressOneTime = false;
    if ((PIND & MASK))
    {
        didPressOneTime = true;
        _delay_ms(DELAY);
    }
    return ((PIND & MASK)) && didPressOneTime;
}

voit initialize()
{
    // cli() et sei() désactivent/activent le status register (SREG).
    // Le SREG au bit l (7) est le Global interrupt enable.
    cli(); // Global interrupt disable

    DDRA |= (1 << PA1) || (1 << PA0);
    // ici pourquoi PD5 pour ocna = Oc1A?? C'est quoi ceci?
    // DDRD |= (1 << PD5);
    DDRD &= ~(1 << PD2);

    // External interrupt mask register génère demande d'interruption lorsque
    // SREG = 1 et un des bits 2-0 du EIMSK = 1. Réf: P. 68.
    EIMSK |= (1 << INT0);

    // External interrupt control register A ajusté à 0 1 (n'importe quel front
    // génère une demande d'interruption) Réf: P. 68.
    EICRA |= (0 << ISC00);
    EICRA &= ~(1 << ISC01);

    sei(); // Global interrupt enable.
}

// CEUX CI PPEUVENT AVOIR BESOIN DE DEBUG SI CA NE MARCHE PAS!!!!
ISR(WDT_vect) // Watchdog timeout interrupt.
{
    gExpiredTimer = 1;
}

ISR(TIMER1_CAPT_vect) // Timer/Counter capture event.
{
    gPushButton = 1;
    didPress(gPushButton);
    if (TCNT1 == TCNT0)
    {
    }
}

void startTimer(uint16_t duree)
{
    gExpiredTimer = 0;
    // Mode CTC du timer 1 avec horloge divisée par 1024
    // interruption après la durée spécifiée.
    TCNT1 = 0;     // POURQUOI 0????
    OCR1A = duree; // Output compare register 1.

    TCCR1A = 'modifier ici';
    TCCR1B = 'modifier ici';
    TCCR1C = 0;
    TIMSK1 = 'modifier ici';
}

int main()
{
    initialize();
    volatile uint8_t gExpiredTimer;
    volatile uint8_t gPushButton;

    do
    {
        // attendre qu'une des deux variables soit modifiée
        // par une ou l'autre des interruptions.
    } while (gExpiredTimer == 0 && gPushButton == 0);

    cli();
    // Verifier la réponse
    'modifier ici'
}