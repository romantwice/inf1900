#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

enum class LEDState 
{ 
    INIT, 
    FIRST_PRESS, 
    FIRST_RELEASE, 
    SECOND_PRESS, 
    SECOND_RELEASE, 
    THIRD_PRESS
};

enum class Color
{ 
    RED, 
    AMBER, 
    GREEN, 
    NONE 
};

const uint8_t MASK = 0x04;
const uint8_t DELAY = 10;

volatile uint8_t gMinuterieExpiree;
volatile uint8_t gBoutonPoussoir;

// Pour initialiser l'état à INIT, donc la DEL en couleur rouge.
volatile LEDState state{LEDState::INIT}; 
LEDState nextState{LEDState::INIT}; 

volatile bool mySwitch = true; 



void changeColor(Color color)
{
    switch (color)
    {
    case Color::RED :
        PORTA &= ~(1 << PA0); // Pour avoir 1111 1110 & 0000 0001 = 0000 0000.
        PORTA |= 1 << PA1;
        break;

    case Color::AMBER :
    {
        PORTA &= ~(1 << PA0); // Pour avoir 1111 1110 & 0000 0001 = 0000 0000.
        PORTA |= 1 << PA1;
        _delay_ms(DELAY);

        PORTA &= ~(1 << PA1); // Pour avoir 1111 1101 & 0000 0010 = 0000 0000
        PORTA |= 1 << PA0;    // 0000 0001
        _delay_ms(DELAY);
        break;
    }
    case Color::GREEN :
        PORTA &= ~(1 << PA1); // Pour avoir 1111 1101 & 0000 0010 = 0000 0000
        PORTA |= 1 << PA0;    // 0000 0001
        break;

    case Color::NONE :
        PORTA &= (0 << PA0) & (0 << PA1);
        break;

    default:
        break;
    }
}

bool didPress(uint8_t MASK)
{
    bool didPressOneTime = false;
    if ((PIND & MASK))
    {
        didPressOneTime = true;
        _delay_ms(DELAY);
    }
    return ((PIND & MASK)) && didPressOneTime;
}

ISR(TIMER1_COMPA_vect) {
    gMinuterieExpiree += 1;
    if (gMinuterieExpiree == 2)
        changeColor(Color::RED);
}


void partirMinuterie(uint16_t duree) {
    gMinuterieExpiree = 0;
    // mode CTC du timer 1 avec horloge divisée par 1024
    // interruption après la durée spécifiée
    TCNT1 = 0;
    OCR1A = duree;

    // Set timer control register, there is 3 control register for that with different purposes

    // Bit 7, 6, 5, 4 allow us to set TCCR1A on compare match to high level (page 128)
    // Bit 1 et 0 (WGMn1 et WGMn0) allow us to set the CTC Mode (page 119)
    TCCR1A = 0b11110000; 

    // Bit 0: Set clock to no prescaling (synchronized with CPU clock) (page 131)
    // Bit 3: Set WGMn2 = 1 to CTC Mode 
    TCCR1B = 0b00001101; // Set clock to no prescaling (synchronized with CPU clock) (page 131)
    TCCR1C = 0b00000000; // Not needed (page 131)

    TIMSK1 = 0b00000010;
}

// Cycles must be between [255, 0]
// 255 cycles means max speed and 0 means min speed
void pwmSetup(uint8_t cycles) {
    // mise à un des sorties OC1A et OC1B sur comparaison
    // réussie en mode PWM 8 bits, phase correcte
    // et valeur de TOP fixe à 0xFF (mode #1 de la table 16-5
    // page 130 de la description technique du ATmega324PA)
    OCR1A = cycles;
    // OCR1B = 250;

    TCCR1A &= ~(1 << COM1A0);
    TCCR1A |= (1 << COM1A1);

    // TCCR1A |= (1 << COM1A1);
    // TCCR1A |= (1 << COM1A0);
    TCCR1A |= (1 << WGM10);
    TCCR1A &= ~(1 << WGM11);

    // division d'horloge par 8 - implique une fréquence de PWM fixe
    TCCR1B &= ~(1 << WGM12);
    TCCR1B &= ~(1 << CS12) & ~(1 << CS10);
    TCCR1B |= (1 << CS11);
    
    TCCR1C = 0;
}

ISR(INT0_vect) {
    // laisser un délai avant de confirmer la réponse du
    // bouton-poussoir: environ 30 ms (anti-rebond)
    // _delay_ms ( 30 );

    // se souvenir ici si le bouton est pressé ou relâché
    // 'modifier ici'

    // changements d'état tels que ceux de la
    // semaine précédente
    // 'modifier ici'  

    pwmSetup(128);
    // changeColor(Color::GREEN);

    // Voir la note plus bas pour comprendre cette instruction et son rôle
    // EIFR |= (1 << INTF0) ;
}

void initialize() {
    cli();                           // Desactiver interruptions
    
    DDRA |= (1 << PA0) | (1 << PA1); // PORT A est en mode sortie.
    DDRD &= ~(1 << PD2);             // Seulement D2 est en mode entrée.

    EIMSK |= (1 << INT0);            // Mask pour checker sur le interrupteur 0 (INT0)

    // Sens de controle de INT0 est sur rising edge et falling edge   
    EICRA |= (1 << ISC00);
    EICRA &= ~(1 << ISC01);

    sei();   
}
int main()
{
    initialize();

    DDRD |= 1 << PD5;
    DDRD |= 1 << PD4;
  
    pwmSetup(255);    
    // _delay_ms(3000);

    // pwmSetup(20);
    // _delay_ms(4000);

    // pwmSetup(191);
    // _delay_ms(3000);

    // pwmSetup(20);
    // _delay_ms(4000);

    // pwmSetup(128);
    // _delay_ms(3000);

    // pwmSetup(20);
    // _delay_ms(4000);

    // pwmSetup(64);
    // _delay_ms(3000);

    // pwmSetup(0);
    // _delay_ms(2000);
    PORTD |= 1 << PD4;
    DDRA |= (1 << PA0) | (1 << PA1);
    changeColor(Color::NONE);
    while (true)
    {
        // PORTA |= 1 << PA0;
    }
}