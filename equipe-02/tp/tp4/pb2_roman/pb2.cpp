//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//
//  Description: Ce programme change les couleurs de la DEL bicolore de la carte AVR du robot dans l'ordre speficique suivant:
//  Au debut la DEL est rouge, lorsque l'interrupteur est pese la premiere fois elle tourne ambre, des que relache
//  elle tournera verte. Par la suite, quand on va peser de nouveau elle sera rouge, et lorsque relaché elle s'éteindra.
//  Finalement, lorsque pesé cette fois-ci la lumière sera verte et dès que le bouton est relâché la lumière
//  revient a l'état initial, donc rouge.
//
//  Identifications matérielles (Broches I/O):
//      D2 est connecté au button de la section interrupt, donc il faut mettre le cavalier sur IntEN.
//      A0 est connecté au positif de la Del libre.
//      A1 est connecté au négatif de la Del libre.
//      Ces trois derniers sont en mode sortie.
//
//                      Tableau d'états
// +----------------+---------+----------------+---------------+
// | Present State  | PRESSED |   Next State   | Output(Color) |
// +----------------+---------+----------------+---------------+
// | INIT           |       0 | INIT           | RED           |
// | INIT           |       1 | FIRST_PRESS    | AMBRE         |
// | FIRST_PRESS    |       0 | FIRST_RELEASE  | GREEN         |
// | FIRST_RELEASE  |       1 | SECOND_PRESS   | RED           |
// | SECOND_PRESS   |       0 | SECOND_RELEASE | NONE          |
// | SECOND_RELEASE |       1 | THIRD_PRESS    | GREEN         |
// | THIRD_PRESS    |       0 | INIT           | RED           |
// +----------------+---------+----------------+---------------+

//  Dès que le bouton est relâché, notre état change au prochain état. 
//  Sur le tableau on voit donc que SECOND_PRESS.
//  équivaut à dire que le bouton est en train d'être appuyé une 2ème fois.

#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

enum class LEDState 
{ 
    INIT, 
    FIRST_PRESS, 
    FIRST_RELEASE, 
    SECOND_PRESS, 
    SECOND_RELEASE, 
    THIRD_PRESS
};

enum class Color
{ 
    RED, 
    AMBER, 
    GREEN, 
    NONE 
};

const uint8_t MASK = 0x04;
const uint8_t DELAY = 10;

volatile uint8_t gMinuterieExpiree;
volatile uint8_t gBoutonPoussoir;

// Pour initialiser l'état à INIT, donc la DEL en couleur rouge.
volatile LEDState state{LEDState::INIT}; 
LEDState nextState{LEDState::INIT}; 

volatile bool mySwitch = true; 



void changeColor(Color color)
{
    switch (color)
    {
    case Color::RED :
        PORTA &= ~(1 << PA0); // Pour avoir 1111 1110 & 0000 0001 = 0000 0000.
        PORTA |= 1 << PA1;
        break;

    case Color::AMBER :
    {
        PORTA &= ~(1 << PA0); // Pour avoir 1111 1110 & 0000 0001 = 0000 0000.
        PORTA |= 1 << PA1;
        _delay_ms(DELAY);

        PORTA &= ~(1 << PA1); // Pour avoir 1111 1101 & 0000 0010 = 0000 0000
        PORTA |= 1 << PA0;    // 0000 0001
        _delay_ms(DELAY);
        break;
    }
    case Color::GREEN :
        PORTA &= ~(1 << PA1); // Pour avoir 1111 1101 & 0000 0010 = 0000 0000
        PORTA |= 1 << PA0;    // 0000 0001
        break;

    case Color::NONE :
        PORTA &= (0 << PA0) & (0 << PA1);
        break;

    default:
        break;
    }
}

bool didPress(uint8_t MASK)
{
    bool didPressOneTime = false;
    if ((PIND & MASK))
    {
        didPressOneTime = true;
        _delay_ms(DELAY);
    }
    return ((PIND & MASK)) && didPressOneTime;
}

ISR(TIMER1_COMPA_vect) {
    // gMinuterieExpiree += 1;
    // if (gMinuterieExpiree == 2)
        changeColor(Color::RED);
}

ISR(INT0_vect) {
    gBoutonPoussoir = 1;
    // anti-rebond
    // TODO: modifier ici 
}

void partirMinuterie(uint16_t duree) {
    gMinuterieExpiree = 0;
    // mode CTC du timer 1 avec horloge divisée par 1024
    // interruption après la durée spécifiée
    TCNT1 = 0;
    OCR1A = duree;

    // Set timer control register (A, B and C), there is 3 control register for that with different purposes

    TCCR1A |= (1 << COM1A1) | (1 << COM1A0) | (1 << COM1B1) | (1 << COM1B0); // set TCCR1A on compare match to high level (page 128)
    TCCR1A &= ~(1 << WGM11) & ~(1 << WGM10); // allow us to set the CTC Mode (page 119)

    TCCR1B |= (1 << WGM12); // allow us to set the CTC Mode (page 119)
    TCCR1B |= (1 << CS12) | (1 << CS10) ; // Set 1024 prescaling (page 131)
    TCCR1B &= ~(1 << CS11); // Set 1024 prescaling  (synchronized with CPU clock) (page 131)
    
    TCCR1C = 0; // Not necessary (page 131)
    
    TIMSK1 |= (1 << OCIE1A);
}

void initialize() {
    cli();                           // Desactiver interruptions
    
    // DDRA |= (1 << PA0) | (1 << PA1); // PORT A est en mode sortie.
    DDRD |= (1 << PD5); // OUTPUT MODE POUR OCNA = OC1A 
    DDRD &= ~(1 << PD2);             // Seulement D2 est en mode entrée.

    EIMSK |= (1 << INT0);            // Mask pour checker sur le interrupteur 0 (INT0)

    // Sens de controle de INT0 est sur rising edge et falling edge   
    EICRA |= (1 << ISC00);
    EICRA &= ~(1 << ISC01);

    sei();   
}

int main()
{
    // initialize();
    changeColor(Color::NONE);
    sei();
    DDRA |= (1 << PA0) | (1 << PA1); // OUTPUT MODE POUR OCNA = OC1A 

    // DDRD &= ~(1 << PD2);   
    partirMinuterie(23438 * 2);
    while (true) {}
}