//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//
//  Description: Ce programme change les couleurs de la DEL bicolore de la carte AVR du robot dans l'ordre speficique suivant:
//  Au debut la DEL est rouge, lorsque l'interrupteur est pese la premiere fois elle tourne ambre, des que relache
//  elle tournera verte. Par la suite, quand on va peser de nouveau elle sera rouge, et lorsque relaché elle s'éteindra.
//  Finalement, lorsque pesé cette fois-ci la lumière sera verte et dès que le bouton est relâché la lumière
//  revient a l'état initial, donc rouge.
//
//  Identifications matérielles (Broches I/O):
//      D2 est connecté au button de la section interrupt, donc il faut mettre le cavalier sur IntEN.
//      A0 est connecté au positif de la Del libre.
//      A1 est connecté au négatif de la Del libre.
//      Ces trois derniers sont en mode sortie.
//
//                      Tableau d'états
// +----------------+---------+----------------+---------------+
// | Present State  | PRESSED |   Next State   | Output(Color) |
// +----------------+---------+----------------+---------------+
// | INIT           |       0 | INIT           | RED           |
// | INIT           |       1 | FIRST_PRESS    | AMBRE         |
// | FIRST_PRESS    |       0 | FIRST_RELEASE  | GREEN         |
// | FIRST_RELEASE  |       1 | SECOND_PRESS   | RED           |
// | SECOND_PRESS   |       0 | SECOND_RELEASE | NONE          |
// | SECOND_RELEASE |       1 | THIRD_PRESS    | GREEN         |
// | THIRD_PRESS    |       0 | INIT           | RED           |
// +----------------+---------+----------------+---------------+

//  Dès que le bouton est relâché, notre état change au prochain état. 
//  Sur le tableau on voit donc que SECOND_PRESS.
//  équivaut à dire que le bouton est en train d'être appuyé une 2ème fois.

#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

const uint8_t MASK = 0x04;
const uint8_t DELAY = 10;

enum class LEDState 
{ 
    INIT, 
    FIRST_PRESS, 
    FIRST_RELEASE, 
    SECOND_PRESS, 
    SECOND_RELEASE, 
    THIRD_PRESS
};

enum class Color
{ 
    RED, 
    AMBER, 
    GREEN, 
    NONE 
};

// Pour initialiser l'état à INIT, donc la DEL en couleur rouge.
volatile LEDState state{LEDState::INIT}; 
LEDState nextState{LEDState::INIT}; 

volatile bool mySwitch = true; 

void changeColor(Color color)
{
    switch (color)
    {
    case Color::RED :
        PORTA &= ~(1 << PA0); 
        PORTA |= 1 << PA1;
        break;

    case Color::AMBER :
    {
        PORTA &= ~(1 << PA0); 
        PORTA |= 1 << PA1;
        _delay_ms(DELAY);

        PORTA &= ~(1 << PA1); 
        PORTA |= 1 << PA0;   
        _delay_ms(DELAY);
        break;
    }
    case Color::GREEN :
        PORTA &= ~(1 << PA1);
        PORTA |= 1 << PA0;   
        break;

    case Color::NONE :
        PORTA &= (0 << PA0) & (0 << PA1);
        break;

    default:
        break;
    }
}

bool didPress(uint8_t MASK)
{
    bool didPressOneTime = false;
    if ((PIND & MASK))
    {
        didPressOneTime = true;
        _delay_ms(DELAY);
    }
    return ((PIND & MASK)) && didPressOneTime;
}

ISR(INT0_vect) {
    // laisser un délai avant de confirmer la réponse du
    // bouton-poussoir: environ 30 ms (anti-rebond)
    // _delay_ms ( 30 );

    // se souvenir ici si le bouton est pressé ou relâché
    // 'modifier ici'

    // changements d'état tels que ceux de la
    // semaine précédente
    // 'modifier ici'  

    state = nextState;

    // Permet d'indiquer qu'une interruption est arrivee (peut etre ca se fait automatiquement donc pas necessaire)
    EIFR |= (1 << INTF0); 
}

// ajouter dans class button
void initialize() {
    cli();                           // Desactiver interruptions
    
    DDRA |= (1 << PA0) | (1 << PA1); // PORT A est en mode sortie.
    DDRD &= ~(1 << PD2);             // Seulement D2 est en mode entrée.

    EIMSK |= (1 << INT0);            // Mask pour checker sur le interrupteur 0 (INT0)

    // Sens de controle de INT0 est sur rising edge et falling edge   
    EICRA |= (1 << ISC00);
    EICRA &= ~(1 << ISC01);

    sei();   
}

int main()
{
    initialize();

    while (true)
    {
        switch (state)
        {
        case LEDState::INIT :
            changeColor(Color::RED);
            nextState = LEDState::FIRST_PRESS;
            break;

        case LEDState::FIRST_PRESS :
            changeColor(Color::AMBER);
            nextState = LEDState::FIRST_RELEASE;
            break;

        case LEDState::FIRST_RELEASE :
            changeColor(Color::GREEN);
            nextState = LEDState::SECOND_PRESS;
            break;

        case LEDState::SECOND_PRESS :
            changeColor(Color::RED);
            nextState = LEDState::SECOND_RELEASE;
            break;

        case LEDState::SECOND_RELEASE :
            changeColor(Color::NONE);
            nextState = LEDState::THIRD_PRESS;
            break;

        case LEDState::THIRD_PRESS :
            changeColor(Color::GREEN);
            nextState = LEDState::INIT;
            break;

        default:
            break;
        }
    }
}