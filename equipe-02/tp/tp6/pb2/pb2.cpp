//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//
//  Description: Ce programme change la couleur de la DEL en fonction de la
//  valeur retournee par le convertisseur analogique numerique (CAN).
//  Si la lumiere est basse, la DEL sera verte si elle est a lumiere ambiante,
//  elle sera ambree et si le niveau de lumiere est trop fort, elle sera rouge.
//
//  Identifications matérielles (Broches I/O):
//  Port B: les pins PB0 et PB1 sont en mode sortie.
//  Port A: les pins PA2 et PA3 sont en mode entree.

#include <avr/io.h>
#include <util/delay.h>
#include "can.h"

using namespace std;

const uint8_t LED_DELAY = 500;
const uint16_t MAX_ADC_VALUE = 255;
const uint16_t ONE_THIRD_ADC_VALUE = 0.33 * MAX_ADC_VALUE;
const uint16_t TWO_THIRDS_ADC_VALUE = 0.66 * MAX_ADC_VALUE;

enum class Color
{
    RED,
    AMBER,
    GREEN,
    NONE
};

void changeColor(Color color)
{
    switch (color)
    {
    case Color::RED:
        PORTB &= ~(1 << PA0);
        PORTB |= 1 << PA1;
        break;

    case Color::AMBER:
    {
        changeColor(Color::RED);
        _delay_ms(LED_DELAY * 1 / 4);
        changeColor(Color::GREEN);
        _delay_ms(LED_DELAY);
        break;
    }

    case Color::GREEN:
        PORTB &= ~(1 << PA1);
        PORTB |= 1 << PA0;
        break;

    case Color::NONE:
        PORTB &= (0 << PA0) & (0 << PA1);
        break;

    default:
        break;
    }
}

void initialiseRegisters()
{
    DDRB |= (1 << PB0) | (1 << PB1);
    DDRA &= ~(1 << PA2) & ~(1 << PA3);
}

void initialisePhotoresistance()
{
    can converter = can();
    uint16_t value = converter.lecture(PA2);
    value = value >> 2;

    if (value < ONE_THIRD_ADC_VALUE)
    {
        changeColor(Color::RED);
    }
    else if (value < TWO_THIRDS_ADC_VALUE)
    {
        changeColor(Color::AMBER);
    }
    else if (value < MAX_ADC_VALUE)
    {
        changeColor(Color::GREEN);
    }
}

int main()
{
    initialiseRegisters();
    
    while (true)
    {
        initialisePhotoresistance();
    }

    return 0;
}