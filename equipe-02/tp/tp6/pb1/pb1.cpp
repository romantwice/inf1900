//  Auteurs: 
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizalez #2089991
// 
//  Description: TP6. Capteurs et conversion analogique/numérique
// 
//  Identifications matérielles (Broches I/O):
//      D2 est connecté au bouton externe, 
//      donc il faut enlever le cavalier sur IntEN.
//      B0 est connecté au négatif de la Del libre.
//      B1 est connecté au positif de la Del libre.
// 
//  Tableau d'états
// +--------------+----------+--------------+------------------+
// |    State     | Pressing |  Next state  |      Output      |
// +--------------+----------+--------------+------------------+
// | START        | 0        | START        | off              |
// | START        | 1        | PRESSING     | off              |
// | PRESSING     | 1        | PRESSING     | off              |
// | PRESSING     | 0        | GREEN_BLINK  | off              |
// | GREEN_BLINK  | X        | WAITING      | GREEN (blinking) |
// | WAITING      | X        | RED_BLINK    | off              |
// | RED_BLINK    | X        | STATIC_GREEN | RED (blinking)   |
// | STATIC_GREEN | X        | START        | GREEN            |
// +--------------+----------+--------------+------------------+
//
//  Pressing: Le fait de peser le bouton ou non, donc cela fait référence 
//  à la valeur de la broche 2 du port D (D2).

#define F_CPU 8000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

enum class LedState { 
    START, 
    PRESSING, 
    GREEN_BLINK, 
    WAITING, 
    RED_BLINK, 
    STATIC_GREEN 
};

enum class Color { 
    NONE, 
    GREEN, 
    RED 
};

volatile LedState ledState { LedState::START };
volatile uint8_t pressTimeCounter = 0; 

const uint8_t DEBOUNCE_DELAY = 30; // ms
const uint8_t BUTTON_MASK = 0x04;
const uint8_t COUNTER_STEP = 10;

ISR(TIMER1_COMPA_vect) {
    pressTimeCounter += COUNTER_STEP;
    TCNT1 = 0;
}

void changeColor(Color color) {
    switch (color)
    {
    case Color::NONE :
        PORTB &= ~(1 << PB0) & ~(1 << PB1);
        break;

    case Color::GREEN :
        PORTB |= 1 << PB0;
        PORTB &= ~(1 << PB1); 
        break;

    case Color::RED :
        PORTB &= ~(1 << PB0);
        PORTB |= 1 << PB1; 
        break;
    
    default:
        break;
    }
}

uint16_t secondsToCycles(uint16_t seconds) {
    const uint16_t cyclesPerSecond = 7813; // Hz 
    return uint16_t(cyclesPerSecond * seconds); 
}

void setSecondsToStop(uint16_t seconds) {
    OCR1A = secondsToCycles(seconds);
}

void setCyclesToStop(uint16_t cycles) {
    OCR1A = cycles;
}


void initializePinsDirection() {
    // Input pin
    DDRD &= ~(1 << PD2); 

    // Output pins
    DDRB |= (1 << PB0) | (1 << PB1);
}

void initializeCounter(uint16_t secondsToStop) {
    TCNT1 = 0;
    setSecondsToStop(secondsToStop);

    // set compare match to high level
    TCCR1A |= (1 << COM1A1) | (1 << COM1A0) | (1 << COM1B1) | (1 << COM1B0);

    // set the CTC Mode
    TCCR1A &= ~(1 << WGM11) & ~(1 << WGM10); 
    TCCR1B |= (1 << WGM12); 

    // Set prescaling 
    TCCR1B |= (1 << CS12) | (1 << CS10) ;
    TCCR1B &= ~(1 << CS11);
    
    TIMSK1 |= (1 << OCIE1A);
}

void initializeHardware() {
    cli();
    initializePinsDirection();
    sei();
}

bool didPress()
{
    bool didAlreadyPress = false;
    if ((PIND & BUTTON_MASK))
    {
        didAlreadyPress = true;
        _delay_ms(DEBOUNCE_DELAY);
    }
    return ((PIND & BUTTON_MASK)) && didAlreadyPress;
}

bool didRelease() {
    return !didPress();
}

void stopTimer() {
    cli();
}

void handleStaticGreenState() {
    const int delay = 1000;
    changeColor(Color::GREEN);
    _delay_ms(delay);
    ledState = LedState::START;
}

inline void makeBlink(Color color, const int delay, int iterations) {
    for (int i = 0; i < iterations; i++) {
        changeColor(color);
        _delay_ms(delay);
        changeColor(Color::NONE);
        _delay_ms(delay);
    } 
}

void handleRedBlinkState() {
    const int delay = 500; // Because 500 * 2 = 1s blinking 
    int iterations = pressTimeCounter / 2;

    makeBlink(Color::RED, delay, iterations);
    ledState = LedState::STATIC_GREEN;   
}

void handleWaitingState() {
    const int delay = 2000;
    _delay_ms(delay);
    ledState = LedState::RED_BLINK;
}

void handleGreenBlinkState() {
    const uint8_t delay = 50;
    const uint8_t iterations = 5; // Because (5 * 50) * 2 = 500ms blinking

    makeBlink(Color::GREEN, delay, iterations);
    ledState = LedState::WAITING;    
}

void handlePressingState(bool didStartCounter) {
    const uint8_t counterLimit = 120;
    const uint8_t secondsToInterrupt = 1;

    if (!didStartCounter) {       
        initializeCounter(secondsToInterrupt);
        changeColor(Color::NONE);
    }

    if (pressTimeCounter >= counterLimit || didRelease()) {
        stopTimer();
        ledState = LedState::GREEN_BLINK;
    }
}

void handleLedState() {
    bool didStartCounter = false;

    while (true)
    {
        switch (ledState)
        {
        case LedState::START:
            changeColor(Color::NONE);
            if (didPress())
                ledState = LedState::PRESSING;
            break;
        
        case LedState::PRESSING:
            handlePressingState(didStartCounter);
            didStartCounter = true;
            break;

        case LedState::GREEN_BLINK:
            handleGreenBlinkState();
            break;

        case LedState::WAITING:
            handleWaitingState();
            break;

        case LedState::RED_BLINK:
            handleRedBlinkState();
            break;

        case LedState::STATIC_GREEN:
            didStartCounter = false;
            sei(); // Turn on interruptions again 
            handleStaticGreenState();
            break;
        
        default:
            break;
        }
    }
}

int main() {
    initializeHardware();
    handleLedState();
}