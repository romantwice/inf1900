//  Auteurs:
//      Rodrigo A. Merino Martel #2152067
//      Roman A. Roman Canizales #2089991
//
//  Description:
//
//  Identifications matérielles (Broches I/O):
//

#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>

const uint8_t MASK = 0x04;
const double PERIOD_1 = (1 / 60000);
const double PERIOD_2 = (1 / 400000);
const uint8_t DELAY = 10;

void activate(double timeActive)
{
    for (int i = 0; i < timeActive; i++)
    {
        _delay_ms(1);
        PORTA |= (1 << PA0);
    }
}

void deactivate()
{
    PORTA &= ~(1 << PA0);
}

double getTimeActive(double period, double percentage)
{
    return period * percentage;
}

void generatePWM(double period)
{
    for (int i = 0; i < period - getTimeActive(PERIOD_1, 0.25); i++)
    {
        _delay_ms(1);
        activate(getTimeActive(PERIOD_1, 0.25));
    }
    deactivate();
}

int main()
{
    DDRA |= (1 << PA0);
    DDRA |= (1 << PA1); 
    for (int i = 0; i < 10000; i++)
    {
        _delay_ms(1); 
        generatePWM(PERIOD_1);
    }
}